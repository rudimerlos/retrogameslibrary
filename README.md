# Retro Games Library


## Introducción

Retro Games Library es un gestor de juegos para varias plataformas de videojuegos. La aplicación te permite listar los juegos, las plataformas y los emuladores. También permite visualizar información y capturas de pantalla de cada juego. Además, el gestor permite lanzar los juegos con su respectivo emulador previamente instalado y configurado en la aplicación.

Por defecto, la aplicación inserta 5 plataformas para poder empezar fácilmente a insertar juegos, pero pueden añadirse todas las que se deseen. Estas plataformas pueden ser eliminadas o editadas. Las plataformas que incorpora son:

- Nintendo NES
- Sega Master System II
- Amstrad CPC 6128
- Spectrum ZX 128
- Atari 2600

La aplicación genera un directorio llamado RetroGamesLibrary en el directorio HOME del usuario donde se alojarán las roms e imágenes añadidas.


## Instalación

Antes de empezar es necesario tener instalado y configurado Java 11 (o superior) y Maven.

Para la compilación y ejecución basta con introducir:

        $ mvn javafx:run

Para la instalación:

        $ mvn package

Esto creará un directorio *out* en el que estarán todos los jars necesarios para ejecutar la aplicación con:

        $ java --module-path out --add-modules javafx.controls,javafx.fxml,com.google.gson -jar out/RetroGamesLibrary-1.2.0.jar


## Descarga de binarios

Existen binarios compilados para Windows (exe), MacOS (dmg) y Linux (deb) [aquí](https://drive.google.com/drive/folders/1Onm3P-XJsNFtNtO2jTpO4it3_Ij4kZm0?usp=sharing).

## Funcionalidades

A continuación se detallan las funcionalidades de la aplicación:

- **Menú archivo**
    - **Añadir nuevo juego**: Abre un diálogo para añadir un nuevo juego a la base de datos Game.
    - **Importar**: Permite importar los datos de una biblioteca de juegos, plataformas y emuladores, así como los recursos de los juegos.
    - **Exportar**: Permite exportar los datos de la colección de juegos, plataformas y emuladores, así como los recursos de los juegos.
    - **Salir**: Sale de la aplicación.
- **Menú editar**
    - **Editar juego**: Abre un diálogo con los datos del juego seleccionado para editarlo.
    - **Eliminar juego**: Muestra un diálogo para confirmar la eliminación del juego seleccionado.
- **Menú ver**
    - **Plataformas**: Abre una ventana con el listado de plataformas y permite añadir, editar y eliminar.
    - **Emuladores**: Abre una ventana con el listado de emuladores y permite añadir, editar y eliminar.
- **Menú opciones**
    - **Preferencias**: Abre una ventana que permite configurar algunos aspectos de la aplicación.
- **Menú ayuda**
    - **Acerca de**: Muestra información de la aplicación como una breve descripción, la versión, el autor, las versiones de java y la licencia.
- **Botón +**: Abre un diálogo para añadir un nuevo juego a la base de datos Game.
- **Botón ►**: Ejecuta el juego en su correspondiente emulador solo si está instalado y configurado.
- **Buscador**: Muestra solo los juegos cuyo título contiene la ocurrencia. Se actualiza a medida que se va introduciendo el texto.
- **Panel derecho**: Muestra toda la información del juego seleccionado.


## Licencia

Copyright (C) 2021  Rudi Merlos Carrión

This file is part of RetroGamesLibrary.

RetroGamesLibrary is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

RetroGamesLibrary is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with RetroGamesLibrary. If not, see https://www.gnu.org/licenses/.