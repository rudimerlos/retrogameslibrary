/***************************************************************************************************
 * Copyright (C) 2021 Rudi Merlos Carrión
 *
 * This file is part of RetroGamesLibrary.
 *
 * RetroGamesLibrary is free software: you can redistribute it and/or modify it under the terms of
 * the GNU General Public License as published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version.
 *
 * RetroGamesLibrary is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with RetroGamesLibrary.
 * If not, see <https://www.gnu.org/licenses/>.
 **************************************************************************************************/

package org.rmc.retrogameslibrary.model;

import java.io.Serializable;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class Platform implements Serializable {

    private static final long serialVersionUID = 1L;

    private Long id;

    private String name;

    private String model;

    private String company;

    private String country;

    private Short year;

    public Platform(String name, String model, String company, String country, Short year) {
        this.name = name;
        this.model = model;
        this.company = company;
        this.country = country;
        this.year = year;
    }

    public Platform(Platform other) {
        this.id = other.id;
        this.name = other.name;
        this.model = other.model;
        this.company = other.company;
        this.country = other.country;
        this.year = other.year;
    }

    @Override
    public String toString() {
        return this.name + " - " + this.model;
    }
}
