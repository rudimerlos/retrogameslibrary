/***************************************************************************************************
 * Copyright (C) 2021 Rudi Merlos Carrión
 *
 * This file is part of RetroGamesLibrary.
 *
 * RetroGamesLibrary is free software: you can redistribute it and/or modify it under the terms of
 * the GNU General Public License as published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version.
 *
 * RetroGamesLibrary is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with RetroGamesLibrary.
 * If not, see <https://www.gnu.org/licenses/>.
 **************************************************************************************************/

package org.rmc.retrogameslibrary.model;

import java.io.IOException;
import java.io.Serializable;
import java.nio.file.Files;
import java.nio.file.Paths;
import org.rmc.retrogameslibrary.config.PropertiesConfig;
import org.rmc.retrogameslibrary.repository.CrudException;
import org.rmc.retrogameslibrary.service.EmulatorService;
import org.rmc.retrogameslibrary.service.PlatformService;
import org.rmc.retrogameslibrary.service.jdbc.JDBCEmulatorService;
import org.rmc.retrogameslibrary.service.jdbc.JDBCPlatformService;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class Game implements Serializable {

    private static final long serialVersionUID = 1L;

    private Long id;

    private String title;

    private String description;

    private Short year;

    private String gender;

    private String screenshot;

    private String path;

    private String preArgs;

    private String postArgs;

    private Long platform_id;

    private Long emulator_id;

    public Game(String title, String description, Short year, String gender, String screenshot,
            String path, String preArgs, String postArgs, Long platform_id, Long emulator_id) {
        this.title = title;
        this.description = description;
        this.year = year;
        this.gender = gender;
        this.screenshot = screenshot;
        this.path = path;
        this.preArgs = preArgs;
        this.postArgs = postArgs;
        this.platform_id = platform_id;
        this.emulator_id = emulator_id;
    }

    public Game(Game other) {
        this.id = other.id;
        this.title = other.title;
        this.description = other.description;
        this.year = other.year;
        this.gender = other.gender;
        this.screenshot = other.screenshot;
        this.path = other.path;
        this.preArgs = other.preArgs;
        this.postArgs = other.postArgs;
        this.platform_id = other.platform_id;
        this.emulator_id = other.emulator_id;
    }

    public String getPlatform_id() {
        String result = null;
        PlatformService platformService = new JDBCPlatformService();
        try {
            Platform platform = platformService.getById(platform_id);
            result = platform.getName() + " - " + platform.getModel();
        } catch (CrudException e) {
            e.printStackTrace();
        }
        return result;
    }

    public Long getPlatform_idId() {
        return this.platform_id;
    }

    public String getEmulator_id() {
        String result = null;
        EmulatorService emulatorService = new JDBCEmulatorService();
        try {
            Emulator emulator = emulatorService.getById(emulator_id);
            if (emulator != null)
                result = emulator.getName();
        } catch (CrudException e) {
            e.printStackTrace();
        }
        return result;
    }

    public Long getEmulator_idId() {
        return this.emulator_id;
    }

    public String getScreenshotPath() {
        return this.screenshot;
    }

    public ImageView getScreenshot() {
        ImageView imageView = null;
        if (this.screenshot != null) {
            if (Files.exists(Paths.get(PropertiesConfig.ROOT_PATH, this.screenshot))) {
                try {
                    imageView = new ImageView(new Image(Files.newInputStream(
                            Paths.get(PropertiesConfig.ROOT_PATH, this.screenshot))));
                    imageView.setPreserveRatio(true);
                    imageView.setFitHeight(56);
                    imageView.setFitWidth(56);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return imageView;
    }

    public String getStringPath() {
        return this.path;
    }

    public ImageView getPath() {
        ImageView imageView = null;
        if (this.path != null && !this.path.isEmpty()
                && Files.exists(Paths.get(PropertiesConfig.ROOT_PATH, getStringPath())))
            imageView = new ImageView(new Image(getClass().getResourceAsStream("/img/yes.png")));
        else
            imageView = new ImageView(new Image(getClass().getResourceAsStream("/img/no.png")));
        imageView.setPreserveRatio(true);
        imageView.setFitHeight(60);
        imageView.setFitWidth(60);
        return imageView;
    }
}
