/***************************************************************************************************
 * Copyright (C) 2021 Rudi Merlos Carrión
 *
 * This file is part of RetroGamesLibrary.
 *
 * RetroGamesLibrary is free software: you can redistribute it and/or modify it under the terms of
 * the GNU General Public License as published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version.
 *
 * RetroGamesLibrary is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with RetroGamesLibrary.
 * If not, see <https://www.gnu.org/licenses/>.
 **************************************************************************************************/

package org.rmc.retrogameslibrary.util;

import java.io.IOException;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Stream;
import org.rmc.retrogameslibrary.config.PropertiesConfig;
import org.rmc.retrogameslibrary.dialog.AppDialog;
import org.rmc.retrogameslibrary.model.Game;
import org.rmc.retrogameslibrary.model.Platform;
import net.lingala.zip4j.ZipFile;
import net.lingala.zip4j.exception.ZipException;

public class DirUtility {

    public static final String SEPARATOR = FileSystems.getDefault().getSeparator();
    public static final String PATH_TO_ROMS = "roms" + SEPARATOR;

    // Return the name of the platform resources directory
    public static String getPlatformDirName(Platform platform) {
        return PATH_TO_ROMS + String.valueOf(platform.getId());
    }

    // Return the name of the game resources directory
    public static String getGameDirName(Game game) {
        return PATH_TO_ROMS + String.valueOf(game.getPlatform_idId()) + SEPARATOR
                + game.getId() + "_" + game.getTitle().toLowerCase().replaceAll(" ", "")
                + SEPARATOR;
    }

    // Check if game resources directory exists
    public static boolean existsGameDir(Game game) {
        return Files.exists(Paths.get(PropertiesConfig.ROOT_PATH, getGameDirName(game)));
    }

    // Delete a directory and subdirectories recursively
    public static void deleteDir(String path) throws IOException {
        Stream<Path> walk = Files.walk(Paths.get(path));
        walk.sorted(Comparator.reverseOrder()).forEach(p -> {
            try {
                Files.delete(p);
            } catch (IOException e) {
                AppDialog.errorDialog("Error",
                        "Se ha producido un error al eliminar el directorio de recursos.");
            } finally {
                walk.close();
            }
        });
    }

    // Return the file name without extension
    public static String getFileNameWithoutExtension(Path path) {
        return path.getFileName().toString().replaceFirst("[.][^.]+$", "");
    }

    // Compress files in ZIP
    public static void compressZIP(String outputFile, List<String> filesToZip) throws ZipException {
        ZipFile zipFile = new ZipFile(outputFile);
        for (String file : filesToZip) {
            if (file != null) {
                if (Files.isDirectory(Paths.get(file)))
                    zipFile.addFolder(Paths.get(file).toFile());
                else
                    zipFile.addFile(file);
            }
        }
    }

    // Decompress files in ZIP
    public static void decompressZIP(String zip, String outputDir) throws ZipException {
        ZipFile zipFile = new ZipFile(zip);
        zipFile.extractAll(outputDir);
    }
}
