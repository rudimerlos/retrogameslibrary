/***************************************************************************************************
 * Copyright (C) 2021 Rudi Merlos Carrión
 *
 * This file is part of RetroGamesLibrary.
 *
 * RetroGamesLibrary is free software: you can redistribute it and/or modify it under the terms of
 * the GNU General Public License as published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version.
 *
 * RetroGamesLibrary is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with RetroGamesLibrary.
 * If not, see <https://www.gnu.org/licenses/>.
 **************************************************************************************************/

package org.rmc.retrogameslibrary;

import java.util.List;
import org.rmc.retrogameslibrary.config.PropertiesConfig;
import org.rmc.retrogameslibrary.dialog.AppDialog;
import org.rmc.retrogameslibrary.model.Platform;
import org.rmc.retrogameslibrary.repository.CrudException;
import org.rmc.retrogameslibrary.service.PlatformService;
import org.rmc.retrogameslibrary.service.jdbc.JDBCConnection;
import org.rmc.retrogameslibrary.service.jdbc.JDBCPlatformService;
import javafx.application.Application;
import javafx.application.HostServices;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

public class Main extends Application {

    public static HostServices hostServices;

    public static void main(String[] args) {
        Application.launch(args);
    }

    @Override
    public void start(Stage stage) throws Exception {
        // Get host services to access to default web browser
        hostServices = getHostServices();

        // Handle events when application is closing
        stage.setOnCloseRequest(event -> {
            event.consume();
            if (AppDialog.confirmationDialog("Salir de Retro Games Library",
                    "¿Estás seguro de que quieres salir de la aplicación?")) {
                try {
                    JDBCConnection.getInstance().closeConnection();
                } catch (CrudException e) {
                    e.printStackTrace();
                }
                javafx.application.Platform.exit();
            }
        });

        // Set default properties
        if (PropertiesConfig.readProperties() == null || PropertiesConfig.readProperties()
                .getProperty(PropertiesConfig.INIT_HELP) == null) {
            PropertiesConfig.writeInitHelpProperties(true);
        }
        if (PropertiesConfig.readProperties() == null || PropertiesConfig.readProperties()
                .getProperty(PropertiesConfig.TOOLTIPS) == null) {
            PropertiesConfig.writeTooltipProperties(true);
        }

        // Init database connections
        try {
            initDatabase();
        } catch (CrudException e) {
            AppDialog.errorDialog(e.getMessage(),
                    "Imposible realizar la conexión a la base de datos.", e.getCause().toString());
        }

        // Init main window
        Parent root = FXMLLoader.load(getClass().getResource("/view/mainwindow.fxml"));
        stage.setTitle("Retro Games Library");
        Scene scene = new Scene(root);
        scene.getStylesheets().add(getClass().getResource("/styles/table_style.css").toString());
        stage.setScene(scene);
        stage.setMinWidth(800);
        stage.setMinHeight(600);
        stage.getIcons().add(new Image(getClass().getResourceAsStream("/img/icon.png")));
        stage.show();

        // Show init help
        if (PropertiesConfig.readProperties().getProperty(PropertiesConfig.INIT_HELP, "true")
                .equals("true")) {
            Stage newStage = new Stage();
            newStage.initOwner(stage);
            newStage.initModality(Modality.WINDOW_MODAL);
            Parent initHelp = FXMLLoader.load(getClass().getResource("/view/inithelp.fxml"));
            newStage.setScene(new Scene(initHelp));
            newStage.setTitle("Ayuda inicial");
            newStage.initStyle(StageStyle.UNDECORATED);
            newStage.show();
        }
    }

    // Get Hibernate connection and fill platforms table with some values
    private void initDatabase() throws CrudException {
        // Get connection
        JDBCConnection.getInstance().connect();
        // Init database
        PlatformService platformService = new JDBCPlatformService();
        platformService.createTables();
        // Fill pltaform table
        List<Platform> platforms = platformService.getAll();
        if (platforms == null || platforms.isEmpty())
            fillPlatforms();
    }

    // Insert some values into platform table
    private void fillPlatforms() {
        PlatformService platformService = new JDBCPlatformService();
        // @formatter:off
        List<Platform> platforms = List.of(
            new Platform("Atari", "2600", "Atari Inc", "USA", (short) 1977),
            new Platform("Spectrum", "ZX", "Sinclair Research Ltd", "UK", (short) 1982),
            new Platform("Nintendo", "NES", "Nintendo Company Ltd", "Japón", (short) 1983),
            new Platform("Amstrad", "CPC", "Amstrad", "UK", (short) 1984),
            new Platform("Sega", "Master System II", "Sega Corporation", "Japón", (short) 1985));
        // @formatter:on
        platforms.forEach(p -> {
            try {
                platformService.insert(p);
            } catch (CrudException e) {
                AppDialog.errorDialog(e.getMessage(), e.getCause().toString());
            }
        });
    }
}
