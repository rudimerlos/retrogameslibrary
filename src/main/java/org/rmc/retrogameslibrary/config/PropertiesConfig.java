/***************************************************************************************************
 * Copyright (C) 2021 Rudi Merlos Carrión
 *
 * This file is part of RetroGamesLibrary.
 *
 * RetroGamesLibrary is free software: you can redistribute it and/or modify it under the terms of
 * the GNU General Public License as published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version.
 *
 * RetroGamesLibrary is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with RetroGamesLibrary.
 * If not, see <https://www.gnu.org/licenses/>.
 **************************************************************************************************/

package org.rmc.retrogameslibrary.config;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Properties;
import org.rmc.retrogameslibrary.dialog.AppDialog;

public class PropertiesConfig {

    // Root path
    public static final String ROOT_PATH = getRootPath();

    // Path to properties file
    private static final Path PROPERTIES_PATH = Paths.get(ROOT_PATH, "config.conf");

    // FileChooser last paths properties
    public static final String GAME_ROM_LAST_PATH = "game_rom_last_path";
    public static final String GAME_IMG_LAST_PATH = "game_img_last_path";
    public static final String EMULATOR_LAST_PATH = "emulator_last_path";

    // Application tooltips
    public static final String TOOLTIPS = "tooltips";

    // Init help
    public static final String INIT_HELP = "init_help";

    private static Properties properties = new Properties();

    // Get root path
    private static String getRootPath() {
        Path path = null;
        try {
            path = Files.createDirectories(
                    Paths.get(System.getProperty("user.home"), "RetroGamesLibrary"));
        } catch (IOException e) {
            AppDialog.errorDialog("Error", "No se ha podido establecer el directorio principal.");
            System.exit(1);
        }
        return path.toString();
    }

    // Write properties into properties file
    private static void saveProperties() {
        try {
            properties.store(Files.newOutputStream(PROPERTIES_PATH),
                    "Retro Games Library Properties");
        } catch (IOException e) {
            AppDialog.errorDialog("Error al guardar la configuración", e.getCause().toString());
        }
    }

    // Set FileChooser game rom last path
    public static void writeGameLastPathProperties(String lastPath) {
        properties.setProperty(GAME_ROM_LAST_PATH, lastPath);
        saveProperties();
    }

    // Set FileChooser game img last path
    public static void writeImgLastPathProperites(String lastPath) {
        properties.setProperty(GAME_IMG_LAST_PATH, lastPath);
        saveProperties();
    }

    // Set FileChooser emulator last path
    public static void writeEmulatorLastPathProperites(String lastPath) {
        properties.setProperty(EMULATOR_LAST_PATH, lastPath);
        saveProperties();
    }

    // Set show tooltips properties
    public static void writeTooltipProperties(boolean active) {
        properties.setProperty(TOOLTIPS, active ? "true" : "false");
        saveProperties();
    }

    // Set init help properties
    public static void writeInitHelpProperties(boolean active) {
        properties.setProperty(INIT_HELP, active ? "true" : "false");
        saveProperties();
    }

    // Read properties
    // If they has been set it returns the properties, if not, it returns null
    public static Properties readProperties() {
        if (Files.exists(PROPERTIES_PATH)) {
            try {
                properties.load(Files.newInputStream(PROPERTIES_PATH));
                return properties;
            } catch (IOException e) {
                AppDialog.errorDialog("Error",
                        "No se ha podido acceder al fichero de configuración");
            }
        }
        return null;
    }
}
