/***************************************************************************************************
 * Copyright (C) 2021 Rudi Merlos Carrión
 *
 * This file is part of RetroGamesLibrary.
 *
 * RetroGamesLibrary is free software: you can redistribute it and/or modify it under the terms of
 * the GNU General Public License as published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version.
 *
 * RetroGamesLibrary is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with RetroGamesLibrary.
 * If not, see <https://www.gnu.org/licenses/>.
 **************************************************************************************************/

package org.rmc.retrogameslibrary.service.jdbc;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import org.rmc.retrogameslibrary.config.PropertiesConfig;
import org.rmc.retrogameslibrary.repository.CrudException;

// Singleton class
public class JDBCConnection {

    private static JDBCConnection instance = null;

    private Connection connection;

    private JDBCConnection() {
        connection = null;
    }

    // Return a JDBCConnection instance
    public static JDBCConnection getInstance() {
        if (instance == null)
            instance = new JDBCConnection();
        return instance;
    }

    // Set H2 connection
    public void connect() throws CrudException {
        try {
            connection = DriverManager.getConnection(
                    "jdbc:h2:" + PropertiesConfig.ROOT_PATH + "/db/retrodb", "retrogameslibrary",
                    "retrogameslibrary");
        } catch (SQLException e) {
            throw new CrudException("Error de SQL", e);
        }
    }

    // Return the Connection
    public Connection getConnection() {
        return connection;
    }

    // Close resources
    public void closeConnection() throws CrudException {
        if (connection != null)
            try {
                connection.close();
            } catch (SQLException e) {
                throw new CrudException("Error de SQL", e);
            }
    }
}
