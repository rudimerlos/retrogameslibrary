/***************************************************************************************************
 * Copyright (C) 2021 Rudi Merlos Carrión
 *
 * This file is part of RetroGamesLibrary.
 *
 * RetroGamesLibrary is free software: you can redistribute it and/or modify it under the terms of
 * the GNU General Public License as published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version.
 *
 * RetroGamesLibrary is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with RetroGamesLibrary.
 * If not, see <https://www.gnu.org/licenses/>.
 **************************************************************************************************/

package org.rmc.retrogameslibrary.service.jdbc;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Types;
import java.util.ArrayList;
import java.util.List;
import org.rmc.retrogameslibrary.model.Game;
import org.rmc.retrogameslibrary.model.Platform;
import org.rmc.retrogameslibrary.repository.CrudException;
import org.rmc.retrogameslibrary.service.GameService;

public class JDBCGameService extends JDBCService implements GameService, CloseResources {

    // SQL queries
    private final String INSERT = "INSERT INTO Game (title, description, year, gender, screenshot, "
            + "path, preargs, postargs, platform_id, emulator_id) VALUES (?, ?, ?, ?, ?, ?, ?, ?,"
            + " ?, ?)";
    private final String INSERTID = "INSERT INTO Game VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
    private final String UPDATE = "UPDATE Game SET title = ?, description = ?, year = ?, gender = "
            + "?, screenshot = ?, path = ?, preargs = ?, postargs = ?, platform_id = ?, "
            + "emulator_id = ? WHERE id = ?";
    private final String DELETEALL = "DELETE FROM Game";
    private final String DELETE = DELETEALL + " WHERE id = ?";
    private final String GETALL = "SELECT id, title, description, year, gender, screenshot, path, "
            + "preargs, postargs, platform_id, emulator_id FROM Game";
    private final String GETBYID = GETALL + " WHERE id = ?";
    private final String SEARCHBYTITLE = GETALL + " WHERE LOWER(title) LIKE ?";
    private final String SEARCHBYTITLEANDPLATFORMID =
            GETALL + " WHERE LOWER(title) LIKE ? AND platform_id = ?";
    private final String GETBYPLATFORM = GETALL + " WHERE platform_id = ?";

    @Override
    public Long insert(Game game) throws CrudException {
        Long id = null;
        try {
            st = connection.prepareStatement(INSERT, Statement.RETURN_GENERATED_KEYS);
            st.setString(1, game.getTitle());
            st.setString(2, game.getDescription());
            if (game.getYear() == null)
                st.setNull(3, Types.NULL);
            else
                st.setShort(3, game.getYear());
            st.setString(4, game.getGender());
            st.setString(5, game.getScreenshotPath());
            st.setString(6, game.getStringPath());
            st.setString(7, game.getPreArgs());
            st.setString(8, game.getPostArgs());
            st.setLong(9, game.getPlatform_idId());
            if (game.getEmulator_idId() == null)
                st.setNull(10, Types.NULL);
            else
                st.setLong(10, game.getEmulator_idId());
            if (st.executeUpdate() == 0)
                throw new CrudException("Es probable que no se haya insertado el registro.");
            rs = st.getGeneratedKeys();
            if (rs.next())
                id = rs.getLong(1);
        } catch (SQLException e) {
            throw new CrudException("Error de SQL", e);
        } finally {
            close(st, rs);
        }
        return id;
    }

    @Override
    public void insertWithId(Game game) throws CrudException {
        try {
            st = connection.prepareStatement(INSERTID);
            st.setLong(1, game.getId());
            st.setString(2, game.getTitle());
            st.setString(3, game.getDescription());
            if (game.getYear() == null)
                st.setNull(4, Types.NULL);
            else
                st.setShort(4, game.getYear());
            st.setString(5, game.getGender());
            st.setString(6, game.getScreenshotPath());
            st.setString(7, game.getStringPath());
            st.setString(8, game.getPreArgs());
            st.setString(9, game.getPostArgs());
            st.setLong(10, game.getPlatform_idId());
            if (game.getEmulator_idId() == null)
                st.setNull(11, Types.NULL);
            else
                st.setLong(11, game.getEmulator_idId());
            if (st.executeUpdate() == 0)
                throw new CrudException("Es probable que no se haya insertado el registro.");
        } catch (SQLException e) {
            throw new CrudException("Error de SQL", e);
        } finally {
            close(st);
        }
    }

    @Override
    public void modify(Game game) throws CrudException {
        try {
            st = connection.prepareStatement(UPDATE);
            st.setString(1, game.getTitle());
            st.setString(2, game.getDescription());
            if (game.getYear() == null)
                st.setNull(3, Types.NULL);
            else
                st.setShort(3, game.getYear());
            st.setString(4, game.getGender());
            st.setString(5, game.getScreenshotPath());
            st.setString(6, game.getStringPath());
            st.setString(7, game.getPreArgs());
            st.setString(8, game.getPostArgs());
            st.setLong(9, game.getPlatform_idId());
            if (game.getEmulator_idId() == null)
                st.setNull(10, Types.NULL);
            else
                st.setLong(10, game.getEmulator_idId());
            st.setLong(11, game.getId());
            if (st.executeUpdate() == 0)
                throw new CrudException("Es probable que no se haya modificado el registro.");
        } catch (SQLException e) {
            throw new CrudException("Error de SQL", e);
        } finally {
            close(st);
        }
    }

    @Override
    public void remove(Game game) throws CrudException {
        try {
            st = connection.prepareStatement(DELETE);
            st.setLong(1, game.getId());
            if (st.executeUpdate() == 0)
                throw new CrudException("Es probable que no se haya eliminado el registro.");
        } catch (SQLException e) {
            throw new CrudException("Error de SQL", e);
        } finally {
            close(st);
        }
    }

    @Override
    public void removeAll() throws CrudException {
        try {
            st = connection.prepareStatement(DELETEALL + " ORDER BY title");
            st.executeUpdate();
        } catch (SQLException e) {
            throw new CrudException("Error de SQL", e);
        } finally {
            close(st);
        }
    }

    @Override
    public Game getById(Long id) throws CrudException {
        Game game = null;
        try {
            st = connection.prepareStatement(GETBYID);
            st.setLong(1, id);
            rs = st.executeQuery();
            if (rs.next())
                game = resultSetToEmulator(rs);
        } catch (SQLException e) {
            throw new CrudException("Error de SQL", e);
        } finally {
            close(st, rs);
        }
        return game;
    }

    @Override
    public List<Game> getAll() throws CrudException {
        List<Game> games = new ArrayList<>();
        try {
            st = connection.prepareStatement(GETALL + " ORDER BY LOWER(title)");
            rs = st.executeQuery();
            while (rs.next())
                games.add(resultSetToEmulator(rs));
        } catch (SQLException e) {
            throw new CrudException("Error de SQL", e);
        } finally {
            close(st, rs);
        }
        return games;
    }

    @Override
    public List<Game> searchByTitle(String title) throws CrudException {
        List<Game> games = new ArrayList<>();
        try {
            st = connection.prepareStatement(SEARCHBYTITLE + " ORDER BY LOWER(title)");
            st.setString(1, title);
            rs = st.executeQuery();
            while (rs.next())
                games.add(resultSetToEmulator(rs));
        } catch (SQLException e) {
            throw new CrudException("Error de SQL", e);
        } finally {
            close(st, rs);
        }
        return games;
    }

    @Override
    public List<Game> searchByTitleAndPlatformId(String title, Long platform_id)
            throws CrudException {
        List<Game> games = new ArrayList<>();
        try {
            st = connection.prepareStatement(SEARCHBYTITLEANDPLATFORMID + " ORDER BY LOWER(title)");
            st.setString(1, title);
            st.setLong(2, platform_id);
            rs = st.executeQuery();
            while (rs.next())
                games.add(resultSetToEmulator(rs));
        } catch (SQLException e) {
            throw new CrudException("Error de SQL", e);
        } finally {
            close(st, rs);
        }
        return games;
    }

    @Override
    public List<Game> getByPlatform(Platform platform) throws CrudException {
        List<Game> games = new ArrayList<>();
        try {
            st = connection.prepareStatement(GETBYPLATFORM + " ORDER BY LOWER(title)");
            st.setLong(1, platform.getId());
            rs = st.executeQuery();
            while (rs.next())
                games.add(resultSetToEmulator(rs));
        } catch (SQLException e) {
            throw new CrudException("Error de SQL", e);
        } finally {
            close(st, rs);
        }
        return games;
    }

    // Convert ResultSet to Emulator object
    private Game resultSetToEmulator(ResultSet rs) throws SQLException {
        Long id = rs.getLong("id");
        String title = rs.getString("title");
        String description = rs.getString("description");
        Short year = (Short) rs.getObject("year");
        String gender = rs.getString("gender");
        String screenshot = rs.getString("screenshot");
        String path = rs.getString("path");
        String preArgs = rs.getString("preargs");
        String postArgs = rs.getString("postargs");
        Long platform_id = rs.getLong("platform_id");
        Long emulator_id = (Long) rs.getObject("emulator_id");
        return new Game(id, title, description, year, gender, screenshot, path, preArgs, postArgs,
                platform_id, emulator_id);
    }
}
