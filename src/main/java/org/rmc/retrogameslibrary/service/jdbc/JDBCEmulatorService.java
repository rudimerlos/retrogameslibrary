/***************************************************************************************************
 * Copyright (C) 2021 Rudi Merlos Carrión
 *
 * This file is part of RetroGamesLibrary.
 *
 * RetroGamesLibrary is free software: you can redistribute it and/or modify it under the terms of
 * the GNU General Public License as published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version.
 *
 * RetroGamesLibrary is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with RetroGamesLibrary.
 * If not, see <https://www.gnu.org/licenses/>.
 **************************************************************************************************/

package org.rmc.retrogameslibrary.service.jdbc;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import org.rmc.retrogameslibrary.model.Emulator;
import org.rmc.retrogameslibrary.repository.CrudException;
import org.rmc.retrogameslibrary.service.EmulatorService;

public class JDBCEmulatorService extends JDBCService implements EmulatorService, CloseResources {

    // SQL queries
    private final String INSERT = "INSERT INTO Emulator (name, path) VALUES (?, ?)";
    private final String INSERTID = "INSERT INTO Emulator VALUES (?, ?, ?)";
    private final String UPDATE = "UPDATE Emulator SET name = ?, path = ? WHERE id = ?";
    private final String DELETEALL = "DELETE FROM Emulator";
    private final String DELETE = DELETEALL + " WHERE id = ?";
    private final String GETALL = "SELECT id, name, path FROM Emulator";
    private final String GETBYID = GETALL + " WHERE id = ?";
    private final String GETBYNAME = GETALL + " WHERE name = ?";

    @Override
    public Long insert(Emulator emulator) throws CrudException {
        Long id = null;
        try {
            st = connection.prepareStatement(INSERT, Statement.RETURN_GENERATED_KEYS);
            st.setString(1, emulator.getName());
            st.setString(2, emulator.getPath());
            if (st.executeUpdate() == 0)
                throw new CrudException("Es probable que no se haya insertado el registro.");
            rs = st.getGeneratedKeys();
            if (rs.next())
                id = rs.getLong(1);
        } catch (SQLException e) {
            throw new CrudException("Error de SQL", e);
        } finally {
            close(st, rs);
        }
        return id;
    }

    @Override
    public void insertWithId(Emulator emulator) throws CrudException {
        try {
            st = connection.prepareStatement(INSERTID);
            st.setLong(1, emulator.getId());
            st.setString(2, emulator.getName());
            st.setString(3, emulator.getPath());
            if (st.executeUpdate() == 0)
                throw new CrudException("Es probable que no se haya insertado el registro.");
        } catch (SQLException e) {
            throw new CrudException("Error de SQL", e);
        } finally {
            close(st);
        }
    }

    @Override
    public void modify(Emulator emulator) throws CrudException {
        try {
            st = connection.prepareStatement(UPDATE);
            st.setString(1, emulator.getName());
            st.setString(2, emulator.getPath());
            st.setLong(3, emulator.getId());
            if (st.executeUpdate() == 0)
                throw new CrudException("Es probable que no se haya modificado el registro.");
        } catch (SQLException e) {
            throw new CrudException("Error de SQL", e);
        } finally {
            close(st);
        }
    }

    @Override
    public void remove(Emulator emulator) throws CrudException {
        try {
            st = connection.prepareStatement(DELETE);
            st.setLong(1, emulator.getId());
            if (st.executeUpdate() == 0)
                throw new CrudException("Es probable que no se haya eliminado el registro.");
        } catch (SQLException e) {
            throw new CrudException("Error de SQL", e);
        } finally {
            close(st);
        }
    }

    @Override
    public void removeAll() throws CrudException {
        try {
            st = connection.prepareStatement(DELETEALL);
            st.executeUpdate();
        } catch (SQLException e) {
            throw new CrudException("Error de SQL", e);
        } finally {
            close(st);
        }
    }

    @Override
    public Emulator getById(Long id) throws CrudException {
        Emulator emulator = null;
        if (id != null) {
            try {
                st = connection.prepareStatement(GETBYID);
                st.setLong(1, id);
                rs = st.executeQuery();
                if (rs.next())
                    emulator = resultSetToEmulator(rs);
            } catch (SQLException e) {
                throw new CrudException("Error de SQL", e);
            } finally {
                close(st, rs);
            }
        }
        return emulator;
    }

    @Override
    public List<Emulator> getAll() throws CrudException {
        List<Emulator> emulators = new ArrayList<>();
        try {
            st = connection.prepareStatement(GETALL + " ORDER BY LOWER(name)");
            rs = st.executeQuery();
            while (rs.next())
                emulators.add(resultSetToEmulator(rs));
        } catch (SQLException e) {
            throw new CrudException("Error de SQL", e);
        } finally {
            close(st, rs);
        }
        return emulators;
    }

    @Override
    public Emulator getByName(String name) throws CrudException {
        Emulator emulator = null;
        try {
            st = connection.prepareStatement(GETBYNAME);
            st.setString(1, name);
            rs = st.executeQuery();
            if (rs.next())
                emulator = resultSetToEmulator(rs);
        } catch (SQLException e) {
            throw new CrudException("Error de SQL", e);
        } finally {
            close(st, rs);
        }
        return emulator;
    }

    // Convert ResultSet to Emulator object
    private Emulator resultSetToEmulator(ResultSet rs) throws SQLException {
        Long id = rs.getLong("id");
        String name = rs.getString("name");
        String path = rs.getString("path");
        return new Emulator(id, name, path);
    }
}
