/***************************************************************************************************
 * Copyright (C) 2021 Rudi Merlos Carrión
 *
 * This file is part of RetroGamesLibrary.
 *
 * RetroGamesLibrary is free software: you can redistribute it and/or modify it under the terms of
 * the GNU General Public License as published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version.
 *
 * RetroGamesLibrary is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with RetroGamesLibrary.
 * If not, see <https://www.gnu.org/licenses/>.
 **************************************************************************************************/

package org.rmc.retrogameslibrary.service.jdbc;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import org.rmc.retrogameslibrary.model.Platform;
import org.rmc.retrogameslibrary.repository.CrudException;
import org.rmc.retrogameslibrary.service.PlatformService;

public class JDBCPlatformService extends JDBCService implements PlatformService, CloseResources {

    // SQL queries
    private final String INSERT = "INSERT INTO Platform (name, model, company, country, year) "
            + "VALUES (?, ?, ?, ?, ?)";
    private final String INSERTID = "INSERT INTO Platform VALUES (?, ?, ?, ?, ?, ?)";
    private final String UPDATE = "UPDATE Platform SET name = ?, model = ?, company = ?, country = "
            + "?, year = ? WHERE id = ?";
    private final String DELETEALL = "DELETE FROM Platform";
    private final String DELETE = DELETEALL + " WHERE id = ?";
    private final String GETALL = "SELECT id, name, model, company, country, year FROM Platform";
    private final String GETBYID = GETALL + " WHERE id = ?";
    private final String GETBYNAMEANDMODEL = GETALL + " WHERE name LIKE ? AND model LIKE ?";

    @Override
    public Long insert(Platform platform) throws CrudException {
        Long id = null;
        try {
            st = connection.prepareStatement(INSERT, Statement.RETURN_GENERATED_KEYS);
            st.setString(1, platform.getName());
            st.setString(2, platform.getModel());
            st.setString(3, platform.getCompany());
            st.setString(4, platform.getCountry());
            st.setShort(5, platform.getYear());
            if (st.executeUpdate() == 0)
                throw new CrudException("Es probable que no se haya insertado el registro.");
            rs = st.getGeneratedKeys();
            if (rs.next())
                id = rs.getLong(1);
        } catch (SQLException e) {
            throw new CrudException("Error de SQL", e);
        } finally {
            close(st, rs);
        }
        return id;
    }

    @Override
    public void insertWithId(Platform platform) throws CrudException {
        try {
            st = connection.prepareStatement(INSERTID);
            st.setLong(1, platform.getId());
            st.setString(2, platform.getName());
            st.setString(3, platform.getModel());
            st.setString(4, platform.getCompany());
            st.setString(5, platform.getCountry());
            st.setShort(6, platform.getYear());
            if (st.executeUpdate() == 0)
                throw new CrudException("Es probable que no se haya insertado el registro.");
        } catch (SQLException e) {
            throw new CrudException("Error de SQL", e);
        } finally {
            close(st);
        }
    }

    @Override
    public void modify(Platform platform) throws CrudException {
        try {
            st = connection.prepareStatement(UPDATE);
            st.setString(1, platform.getName());
            st.setString(2, platform.getModel());
            st.setString(3, platform.getCompany());
            st.setString(4, platform.getCountry());
            st.setShort(5, platform.getYear());
            st.setLong(6, platform.getId());
            if (st.executeUpdate() == 0)
                throw new CrudException("Es probable que no se haya modificado el registro.");
        } catch (SQLException e) {
            throw new CrudException("Error de SQL", e);
        } finally {
            close(st);
        }
    }

    @Override
    public void remove(Platform platform) throws CrudException {
        try {
            st = connection.prepareStatement(DELETE);
            st.setLong(1, platform.getId());
            if (st.executeUpdate() == 0)
                throw new CrudException("Es probable que no se haya eliminado el registro.");
        } catch (SQLException e) {
            throw new CrudException("Error de SQL", e);
        } finally {
            close(st);
        }
    }

    @Override
    public void removeAll() throws CrudException {
        try {
            st = connection.prepareStatement(DELETEALL);
            st.executeUpdate();
        } catch (SQLException e) {
            throw new CrudException("Error de SQL", e);
        } finally {
            close(st);
        }
    }

    @Override
    public Platform getById(Long id) throws CrudException {
        Platform platform = null;
        try {
            st = connection.prepareStatement(GETBYID);
            st.setLong(1, id);
            rs = st.executeQuery();
            if (rs.next())
                platform = resultSetToEmulator(rs);
        } catch (SQLException e) {
            throw new CrudException("Error de SQL", e);
        } finally {
            close(st, rs);
        }
        return platform;
    }

    @Override
    public List<Platform> getAll() throws CrudException {
        List<Platform> platforms = new ArrayList<>();
        try {
            st = connection.prepareStatement(GETALL + " ORDER BY LOWER(name)");
            rs = st.executeQuery();
            while (rs.next())
                platforms.add(resultSetToEmulator(rs));
        } catch (SQLException e) {
            throw new CrudException("Error de SQL", e);
        } finally {
            close(st, rs);
        }
        return platforms;
    }

    @Override
    public Platform getByNameAndModel(String name, String model) throws CrudException {
        Platform platform = null;
        try {
            st = connection.prepareStatement(GETBYNAMEANDMODEL);
            st.setString(1, name);
            st.setString(2, model);
            rs = st.executeQuery();
            if (rs.next())
                platform = resultSetToEmulator(rs);
        } catch (SQLException e) {
            throw new CrudException("Error de SQL", e);
        } finally {
            close(st, rs);
        }
        return platform;
    }

    // Convert ResultSet to Emulator object
    private Platform resultSetToEmulator(ResultSet rs) throws SQLException {
        Long id = rs.getLong("id");
        String name = rs.getString("name");
        String model = rs.getString("model");
        String company = rs.getString("company");
        String country = rs.getString("country");
        Short year = rs.getShort("year");
        return new Platform(id, name, model, company, country, year);
    }

    @Override
    public void createTables() throws CrudException {
        String query = createQuery();
        Statement st = null;
        try {
            st = connection.createStatement();
            if (st.executeUpdate(query) != 0)
                throw new CrudException("Error al crear la base de datos.");
        } catch (SQLException e) {
            throw new CrudException("Error de SQL", e);
        } finally {
            close(st);
        }
    }

    private String createQuery() {
        // @formatter:off
        return "CREATE TABLE IF NOT EXISTS Platform (" +
               "    id IDENTITY PRIMARY KEY," +
               "    name VARCHAR(100) NOT NULL," +
               "    model VARCHAR(50) NOT NULL," +
               "    company VARCHAR(100) NOT NULL," +
               "    country VARCHAR(50)," +
               "    year SMALLINT NOT NULL" +
               ");\n" +
               "CREATE TABLE IF NOT EXISTS Emulator (" +
               "    id IDENTITY PRIMARY KEY," +
               "    name VARCHAR(100) NOT NULL," +
               "    path VARCHAR(255) NOT NULL" +
               ");\n" +
               "CREATE TABLE IF NOT EXISTS Game (" +
               "    id IDENTITY PRIMARY KEY," +
               "    title VARCHAR(100) NOT NULL," +
               "    description CLOB," +
               "    year SMALLINT," +
               "    gender VARCHAR(50)," +
               "    screenshot VARCHAR(255)," +
               "    path VARCHAR(255)," +
               "    preargs VARCHAR(255)," +
               "    postargs VARCHAR(255)," +
               "    platform_id BIGINT NOT NULL," +
               "    emulator_id BIGINT," +
               "    CONSTRAINT game_fk_platform FOREIGN KEY(platform_id) REFERENCES Platform(id) " +
               "        ON DELETE CASCADE," +
               "    CONSTRAINT game_fk_emulator FOREIGN KEY(emulator_id) REFERENCES Emulator(id)" +
               ");";
        // @formatter:on
    }
}
