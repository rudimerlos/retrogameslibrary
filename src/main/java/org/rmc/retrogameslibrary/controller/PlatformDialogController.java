/***************************************************************************************************
 * Copyright (C) 2021 Rudi Merlos Carrión
 *
 * This file is part of RetroGamesLibrary.
 *
 * RetroGamesLibrary is free software: you can redistribute it and/or modify it under the terms of
 * the GNU General Public License as published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version.
 *
 * RetroGamesLibrary is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with RetroGamesLibrary.
 * If not, see <https://www.gnu.org/licenses/>.
 **************************************************************************************************/

package org.rmc.retrogameslibrary.controller;

import java.io.IOException;
import org.rmc.retrogameslibrary.config.PropertiesConfig;
import org.rmc.retrogameslibrary.dialog.AppDialog;
import org.rmc.retrogameslibrary.model.Platform;
import org.rmc.retrogameslibrary.repository.CrudException;
import org.rmc.retrogameslibrary.service.PlatformService;
import org.rmc.retrogameslibrary.service.jdbc.JDBCPlatformService;
import org.rmc.retrogameslibrary.util.DirUtility;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Bounds;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ContextMenu;
import javafx.scene.control.MenuItem;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableRow;
import javafx.scene.control.TableView;
import javafx.scene.control.Tooltip;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyCodeCombination;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.util.Duration;

public class PlatformDialogController {

    @FXML
    private TableView<Platform> tablePlatforms;
    @FXML
    private TableColumn<Platform, String> colNamePlatform;
    @FXML
    private TableColumn<Platform, String> colModelPlatform;
    @FXML
    private TableColumn<Platform, String> colCompanyPlatform;
    @FXML
    private TableColumn<Platform, String> colCountryPlatform;
    @FXML
    private TableColumn<Platform, Integer> colYearPlatform;

    @FXML
    private Button btnAddPlatform;
    @FXML
    private Button btnEditPlatform;
    @FXML
    private Button btnDeletePlatform;

    @FXML
    private Tooltip tipAddPlatform;
    @FXML
    private Tooltip tipEditPlatform;
    @FXML
    private Tooltip tipDeletePlatform;

    private ContextMenu contextMenu;

    private ObjectProperty<TableRow<Platform>> lastSelectedRow;

    @FXML
    public void initialize() {
        showPlatforms();

        createContextMenu();

        // Store the last row selected
        lastSelectedRow = new SimpleObjectProperty<>();
        tablePlatforms.setRowFactory(table -> {
            TableRow<Platform> row = new TableRow<>();
            row.selectedProperty().addListener((observable, oldValue, newValue) -> {
                if (newValue)
                    lastSelectedRow.set(row);
            });
            return row;
        });

        tablePlatforms.getSelectionModel().selectedItemProperty()
                .addListener((observable, oldValue, newValue) -> {
                    if (newValue != null) {
                        tablePlatforms.setContextMenu(contextMenu);
                        // When a game is selected, enables edit and delete buttons
                        btnEditPlatform.setDisable(false);
                        btnDeletePlatform.setDisable(false);
                    } else {
                        tablePlatforms.setContextMenu(null);
                        btnEditPlatform.setDisable(true);
                        btnDeletePlatform.setDisable(true);
                    }
                });

        // Set tooltips visibility
        String propertiesTooltip =
                PropertiesConfig.readProperties().getProperty(PropertiesConfig.TOOLTIPS, "true");
        setTooltips(propertiesTooltip.equals("true"));
    }

    // Set tooltips duration and visibility
    private void setTooltips(boolean active) {
        tipAddPlatform.setShowDelay(Duration.millis(100));
        tipEditPlatform.setShowDelay(Duration.millis(100));
        tipDeletePlatform.setShowDelay(Duration.millis(100));
        if (active) {
            btnAddPlatform.setTooltip(tipAddPlatform);
            btnEditPlatform.setTooltip(tipEditPlatform);
            btnDeletePlatform.setTooltip(tipDeletePlatform);
        } else {
            btnAddPlatform.setTooltip(null);
            btnEditPlatform.setTooltip(null);
            btnDeletePlatform.setTooltip(null);
        }
    }

    // Create a context menu with the edit and delete options
    private void createContextMenu() {
        MenuItem contextEditPlatform = new MenuItem("Editar");
        contextEditPlatform.setOnAction(e -> editPlatform());
        MenuItem contextDeletePlatform = new MenuItem("Eliminar");
        contextDeletePlatform.setAccelerator(new KeyCodeCombination(KeyCode.DELETE));
        contextDeletePlatform.setOnAction(e -> deletePlatform());

        contextMenu = new ContextMenu();
        contextMenu.getItems().addAll(contextEditPlatform, contextDeletePlatform);
    }

    // Read platforms from database and they set into ObservableList
    private ObservableList<Platform> getPlatformList() {
        PlatformService platformService = new JDBCPlatformService();
        ObservableList<Platform> platformList = FXCollections.observableArrayList();
        try {
            platformList.setAll(platformService.getAll());
        } catch (CrudException e) {
            AppDialog.errorDialog(e.getMessage(), e.getCause().toString());
        }
        return platformList;
    }

    // Show platforms in the platforms table
    private void showPlatforms() {
        ObservableList<Platform> platforms = getPlatformList();

        colNamePlatform.setCellValueFactory(new PropertyValueFactory<Platform, String>("name"));
        colModelPlatform.setCellValueFactory(new PropertyValueFactory<Platform, String>("model"));
        colCompanyPlatform
                .setCellValueFactory(new PropertyValueFactory<Platform, String>("company"));
        colCountryPlatform
                .setCellValueFactory(new PropertyValueFactory<Platform, String>("country"));
        colYearPlatform.setCellValueFactory(new PropertyValueFactory<Platform, Integer>("year"));

        tablePlatforms.setItems(platforms);
        tablePlatforms.refresh();
    }

    @FXML
    private void onClickBtnAddPlatform(ActionEvent event) throws IOException {
        Stage stage = (Stage) btnAddPlatform.getScene().getWindow();
        platformEditWindow(stage, null).setOnHidden(e -> showPlatforms());
    }

    @FXML
    private void onClickBtnEditPlatform(ActionEvent event) {
        editPlatform();
    }

    @FXML
    private void onMouseClickedCol(MouseEvent event) {
        // If empty row is selected, clear selection
        if (lastSelectedRow.get() != null) {
            Bounds bounds =
                    lastSelectedRow.get().localToScene(lastSelectedRow.get().getLayoutBounds());
            if (bounds.contains(event.getSceneX(), event.getSceneY()) == false)
                tablePlatforms.getSelectionModel().clearSelection();
        }
        // If double click in a selection, edit platform
        if (event.getButton() == MouseButton.PRIMARY && event.getClickCount() > 1)
            editPlatform();
    }

    @FXML
    private void onKeyReleasedCol(KeyEvent event) {
        if (event.getCode() == KeyCode.ENTER)
            editPlatform();
    }

    // Show platform edit window with the selected platform and show platforms updated when is
    // finished
    private void editPlatform() {
        Platform platform = tablePlatforms.getSelectionModel().getSelectedItem();
        if (platform != null) {
            Stage stage = (Stage) btnEditPlatform.getScene().getWindow();
            try {
                platformEditWindow(stage, platform).setOnHidden(e -> showPlatforms());
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    @FXML
    private void onClickBtnDeletePlatform(ActionEvent event) {
        deletePlatform();
    }

    // Delete platform and all games associated with it
    private void deletePlatform() {
        Platform platform = tablePlatforms.getSelectionModel().getSelectedItem();
        if (platform != null) {
            if (AppDialog.confirmationDialog("Eliminar plataformas",
                    "¿Estás seguro de que quieres borrar la plataforma " + platform.getName() + " "
                            + platform.getModel() + "?",
                    "Ten en cuenta que esta acción eliminará todos los juegos asociados con esta "
                            + "plataforma.")) {
                PlatformService platformService = new JDBCPlatformService();
                try {
                    platformService.remove(platform);
                    AppDialog.messageDialog("Eliminar plataformas", "Se ha eliminado la plataforma "
                            + platform.getName() + " " + platform.getModel() + " con éxito.");
                    showPlatforms();
                    // When a platform is deleted, disables edit and delete buttons
                    btnEditPlatform.setDisable(true);
                    btnDeletePlatform.setDisable(true);
                    // Delete directories
                    DirUtility.deleteDir(DirUtility.getPlatformDirName(platform));
                } catch (CrudException e) {
                    AppDialog.errorDialog(e.getMessage(), e.getCause().toString());
                } catch (IOException e) {
                    AppDialog.errorDialog("Error", "Se ha producido un error al eliminar el "
                            + "directorio de recursos de la plataforma.");
                }
            }
        }
    }

    // Show platform edit window
    private Stage platformEditWindow(Stage stage, Platform platform) throws IOException {
        Stage newStage = new Stage();
        newStage.initOwner(stage);
        newStage.initModality(Modality.WINDOW_MODAL);
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/view/platformeditdialog.fxml"));
        Parent root = loader.load();
        PlatformEditDialogController controller = loader.getController();
        controller.init(platform);
        newStage.setScene(new Scene(root));
        newStage.setTitle("Edición de plataformas");
        newStage.setResizable(false);
        newStage.show();
        return newStage;
    }
}
