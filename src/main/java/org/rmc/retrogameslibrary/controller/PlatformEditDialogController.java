/***************************************************************************************************
 * Copyright (C) 2021 Rudi Merlos Carrión
 *
 * This file is part of RetroGamesLibrary.
 *
 * RetroGamesLibrary is free software: you can redistribute it and/or modify it under the terms of
 * the GNU General Public License as published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version.
 *
 * RetroGamesLibrary is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with RetroGamesLibrary.
 * If not, see <https://www.gnu.org/licenses/>.
 **************************************************************************************************/

package org.rmc.retrogameslibrary.controller;

import org.rmc.retrogameslibrary.dialog.AppDialog;
import org.rmc.retrogameslibrary.model.Platform;
import org.rmc.retrogameslibrary.repository.CrudException;
import org.rmc.retrogameslibrary.service.PlatformService;
import org.rmc.retrogameslibrary.service.jdbc.JDBCPlatformService;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

public class PlatformEditDialogController {

    @FXML
    private Label lblTitle;
    @FXML
    private TextField txtName;
    @FXML
    private TextField txtModel;
    @FXML
    private TextField txtCompany;
    @FXML
    private TextField txtCountry;
    @FXML
    private TextField txtYear;

    @FXML
    private Button btnSave;
    @FXML
    private Button btnCancel;

    private Platform platform; // new platform data
    private Platform oldPlatform; // platform data before change them

    // Result message: 'creada' | 'modificada'
    private String result;

    // Sets the platform to edit
    public void init(Platform platform) {
        if (platform != null) {
            this.platform = platform;
            this.oldPlatform = new Platform(platform);
            lblTitle.setText("Edición de plataforma");
            txtName.setText(platform.getName());
            txtModel.setText(platform.getModel());
            txtCompany.setText(platform.getCompany());
            txtCountry.setText(platform.getCountry());
            txtYear.setText(String.valueOf(platform.getYear()));
            result = "modificada";
        } else {
            this.platform = new Platform();
            lblTitle.setText("Registro de nueva plataforma");
            result = "creada";
        }
    }

    @FXML
    private void onClickBtnSave(ActionEvent event) {
        String name = txtName.getText();
        String model = txtModel.getText();
        String company = txtCompany.getText();
        String country = txtCountry.getText();
        String strYear = txtYear.getText();

        if (!name.isEmpty() && !model.isEmpty() && !company.isEmpty() && !strYear.isEmpty()) {
            if (!yearIsValid(strYear)) {
                AppDialog.errorDialog("Error en el año", "Debes de introducir un año válido.");
            } else {
                Short year = Short.parseShort(strYear);
                PlatformService platformService = new JDBCPlatformService();
                try {
                    platform.setName(name);
                    platform.setModel(model);
                    platform.setCompany(company);
                    platform.setCountry(country);
                    platform.setYear(year);
                    // If platform has been edited
                    if (oldPlatform == null || !oldPlatform.equals(platform)) {
                        if (oldPlatform == null)
                            platformService.insert(platform);
                        else
                            platformService.modify(platform);
                        AppDialog.messageDialog("Edición de plataforma",
                                "Plataforma " + name + " " + model + " " + result + " con éxito.");
                        Stage stage = (Stage) btnSave.getScene().getWindow();
                        stage.close();
                    }
                } catch (CrudException e) {
                    AppDialog.errorDialog(e.getMessage(), e.getCause().toString());
                }
            }
        } else {
            AppDialog.errorDialog("Error en la edición de plataformas",
                    "Debes rellenar los campos obligatorios.");
        }
    }

    @FXML
    private void onClickBtnCancel(ActionEvent event) {
        Stage stage = (Stage) btnCancel.getScene().getWindow();
        stage.close();
    }

    // Checks if year is valid
    private boolean yearIsValid(String year) {
        String regex = "^[12][0-9]{3}$";
        return year.matches(regex);
    }
}
