/***************************************************************************************************
 * Copyright (C) 2021 Rudi Merlos Carrión
 *
 * This file is part of RetroGamesLibrary.
 *
 * RetroGamesLibrary is free software: you can redistribute it and/or modify it under the terms of
 * the GNU General Public License as published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version.
 *
 * RetroGamesLibrary is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with RetroGamesLibrary.
 * If not, see <https://www.gnu.org/licenses/>.
 **************************************************************************************************/

package org.rmc.retrogameslibrary.controller;

import java.io.IOException;
import org.rmc.retrogameslibrary.config.PropertiesConfig;
import org.rmc.retrogameslibrary.dialog.AppDialog;
import org.rmc.retrogameslibrary.model.Emulator;
import org.rmc.retrogameslibrary.repository.CrudException;
import org.rmc.retrogameslibrary.service.EmulatorService;
import org.rmc.retrogameslibrary.service.jdbc.JDBCEmulatorService;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Bounds;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ContextMenu;
import javafx.scene.control.Label;
import javafx.scene.control.MenuItem;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableRow;
import javafx.scene.control.TableView;
import javafx.scene.control.Tooltip;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyCodeCombination;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.util.Duration;

public class EmulatorDialogController {

    @FXML
    private TableView<Emulator> tableEmulators;
    @FXML
    private TableColumn<Emulator, String> colNameEmulator;
    @FXML
    private TableColumn<Emulator, String> colPathEmulator;

    @FXML
    private Button btnAddEmulator;
    @FXML
    private Button btnEditEmulator;
    @FXML
    private Button btnDeleteEmulator;

    @FXML
    private Tooltip tipAddEmulator;
    @FXML
    private Tooltip tipEditEmulator;
    @FXML
    private Tooltip tipDeleteEmulator;

    private ContextMenu contextMenu;

    private ObjectProperty<TableRow<Emulator>> lastSelectedRow;

    @FXML
    public void initialize() {
        tableEmulators.setPlaceholder(new Label("No hay emuladores"));
        showEmulators();

        createContextMenu();

        // Store the last row selected
        lastSelectedRow = new SimpleObjectProperty<>();
        tableEmulators.setRowFactory(table -> {
            TableRow<Emulator> row = new TableRow<>();
            row.selectedProperty().addListener((observable, oldValue, newValue) -> {
                if (newValue)
                    lastSelectedRow.set(row);
            });
            return row;
        });

        tableEmulators.getSelectionModel().selectedItemProperty()
                .addListener((observable, oldValue, newValue) -> {
                    if (newValue != null) {
                        tableEmulators.setContextMenu(contextMenu);
                        // When a game is selected, enables edit and delete buttons
                        btnEditEmulator.setDisable(false);
                        btnDeleteEmulator.setDisable(false);
                    } else {
                        tableEmulators.setContextMenu(null);
                        btnEditEmulator.setDisable(true);
                        btnDeleteEmulator.setDisable(true);
                    }
                });

        // Set tooltips visibility
        String propertiesTooltip =
                PropertiesConfig.readProperties().getProperty(PropertiesConfig.TOOLTIPS, "true");
        setTooltips(propertiesTooltip.equals("true"));
    }

    // Create a context menu with the edit and delete options
    private void createContextMenu() {
        MenuItem contextEditEmulator = new MenuItem("Editar");
        contextEditEmulator.setOnAction(e -> editEmulator());
        MenuItem contextDeleteEmulator = new MenuItem("Eliminar");
        contextDeleteEmulator.setAccelerator(new KeyCodeCombination(KeyCode.DELETE));
        contextDeleteEmulator.setOnAction(e -> deleteEmulator());

        contextMenu = new ContextMenu();
        contextMenu.getItems().addAll(contextEditEmulator, contextDeleteEmulator);
    }

    // Set tooltips duration and visibility
    private void setTooltips(boolean active) {
        tipAddEmulator.setShowDelay(Duration.millis(100));
        tipEditEmulator.setShowDelay(Duration.millis(100));
        tipDeleteEmulator.setShowDelay(Duration.millis(100));
        if (active) {
            btnAddEmulator.setTooltip(tipAddEmulator);
            btnEditEmulator.setTooltip(tipEditEmulator);
            btnDeleteEmulator.setTooltip(tipDeleteEmulator);
        } else {
            btnAddEmulator.setTooltip(null);
            btnEditEmulator.setTooltip(null);
            btnDeleteEmulator.setTooltip(null);
        }
    }

    // Read emulators from database and they set into ObservableList
    private ObservableList<Emulator> getEmulatorList() {
        EmulatorService emulatorService = new JDBCEmulatorService();
        ObservableList<Emulator> emulatorList = FXCollections.observableArrayList();
        try {
            emulatorList.setAll(emulatorService.getAll());
        } catch (CrudException e) {
            AppDialog.errorDialog(e.getMessage(), e.getCause().toString());
        }
        return emulatorList;
    }

    // Show emulators in the emulators table
    private void showEmulators() {
        ObservableList<Emulator> emulators = getEmulatorList();

        colNameEmulator.setCellValueFactory(new PropertyValueFactory<Emulator, String>("name"));
        colPathEmulator.setCellValueFactory(new PropertyValueFactory<Emulator, String>("path"));

        tableEmulators.setItems(emulators);
        tableEmulators.refresh();
    }

    @FXML
    private void onClickBtnAddEmulator(ActionEvent event) throws IOException {
        Stage stage = (Stage) btnAddEmulator.getScene().getWindow();
        emulatorEditWindow(stage, null).setOnHidden(e -> showEmulators());
    }

    @FXML
    private void onClickBtnEditEmulator(ActionEvent event) throws IOException {
        editEmulator();
    }

    @FXML
    private void onMouseClickedCol(MouseEvent event) throws IOException {
        // If empty row is selected, clear selection
        if (lastSelectedRow.get() != null) {
            Bounds bounds =
                    lastSelectedRow.get().localToScene(lastSelectedRow.get().getLayoutBounds());
            if (bounds.contains(event.getSceneX(), event.getSceneY()) == false)
                tableEmulators.getSelectionModel().clearSelection();
        }
        // If double click in a selection, edit game
        if (event.getButton() == MouseButton.PRIMARY && event.getClickCount() > 1)
            editEmulator();
    }

    @FXML
    private void onKeyReleasedCol(KeyEvent event) throws IOException {
        if (event.getCode() == KeyCode.ENTER)
            editEmulator();
    }

    private void editEmulator() {
        Emulator emulator = tableEmulators.getSelectionModel().getSelectedItem();
        if (emulator != null) {
            Stage stage = (Stage) btnEditEmulator.getScene().getWindow();
            try {
                emulatorEditWindow(stage, emulator).setOnHidden(e -> showEmulators());
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    @FXML
    private void onClickBtnDeleteEmulator(ActionEvent event) {
        deleteEmulator();
    }

    private void deleteEmulator() {
        Emulator emulator = tableEmulators.getSelectionModel().getSelectedItem();
        if (emulator != null) {
            if (AppDialog.confirmationDialog("Eliminar emuladores",
                    "¿Estás seguro de que quieres borrar el emulador " + emulator.getName()
                            + "?")) {
                EmulatorService emulatorService = new JDBCEmulatorService();
                try {
                    emulatorService.remove(emulator);
                    AppDialog.messageDialog("Eliminar emuladores",
                            "Se ha eliminado el emulador " + emulator.getName() + " con éxito.");
                    showEmulators();
                    // When a emulator is deleted, disables edit and delete buttons
                    btnEditEmulator.setDisable(true);
                    btnDeleteEmulator.setDisable(true);
                } catch (CrudException e) {
                    AppDialog.errorDialog(e.getMessage(), e.getCause().toString());
                }
            }
        }
    }

    // Show emulator edit window
    private Stage emulatorEditWindow(Stage stage, Emulator emulator) throws IOException {
        Stage newStage = new Stage();
        newStage.initOwner(stage);
        newStage.initModality(Modality.WINDOW_MODAL);
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/view/emulatoreditdialog.fxml"));
        Parent root = loader.load();
        EmulatorEditDialogController controller = loader.getController();
        controller.init(emulator);
        newStage.setScene(new Scene(root));
        newStage.setTitle("Edición de emuladores");
        newStage.setResizable(false);
        newStage.show();
        return newStage;
    }
}
