/***************************************************************************************************
 * Copyright (C) 2021 Rudi Merlos Carrión
 *
 * This file is part of RetroGamesLibrary.
 *
 * RetroGamesLibrary is free software: you can redistribute it and/or modify it under the terms of
 * the GNU General Public License as published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version.
 *
 * RetroGamesLibrary is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with RetroGamesLibrary.
 * If not, see <https://www.gnu.org/licenses/>.
 **************************************************************************************************/

package org.rmc.retrogameslibrary.controller;

import org.rmc.retrogameslibrary.config.PropertiesConfig;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.stage.Stage;

public class InitHelpController {

    @FXML
    private ImageView imgInitHelp;
    @FXML
    private CheckBox chkInitHelp;
    @FXML
    private Button btnClose;

    @FXML
    public void initialize() {
        imgInitHelp.setImage(new Image(getClass().getResourceAsStream("/img/help.png")));
    }

    @FXML
    private void onClickBtnClose(ActionEvent event) {
        PropertiesConfig.writeInitHelpProperties(!chkInitHelp.isSelected());
        Stage stage = (Stage) btnClose.getScene().getWindow();
        stage.close();
    }
}
