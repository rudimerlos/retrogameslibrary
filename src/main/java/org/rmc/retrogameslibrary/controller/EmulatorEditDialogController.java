/***************************************************************************************************
 * Copyright (C) 2021 Rudi Merlos Carrión
 *
 * This file is part of RetroGamesLibrary.
 *
 * RetroGamesLibrary is free software: you can redistribute it and/or modify it under the terms of
 * the GNU General Public License as published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version.
 *
 * RetroGamesLibrary is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with RetroGamesLibrary.
 * If not, see <https://www.gnu.org/licenses/>.
 **************************************************************************************************/

package org.rmc.retrogameslibrary.controller;

import java.io.File;
import java.util.Properties;
import org.rmc.retrogameslibrary.config.PropertiesConfig;
import org.rmc.retrogameslibrary.dialog.AppDialog;
import org.rmc.retrogameslibrary.model.Emulator;
import org.rmc.retrogameslibrary.repository.CrudException;
import org.rmc.retrogameslibrary.service.EmulatorService;
import org.rmc.retrogameslibrary.service.jdbc.JDBCEmulatorService;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.stage.FileChooser;
import javafx.stage.Stage;

public class EmulatorEditDialogController {

    @FXML
    private Label lblTitle;
    @FXML
    private TextField txtNameEmulator;

    @FXML
    private Button btnSelectPathEmulator;
    @FXML
    private Label lblPathEmulator;

    @FXML
    private Button btnCancel;
    @FXML
    private Button btnSave;

    private Emulator emulator; // new emulator data
    private Emulator oldEmulator; // emulator data before change them

    private File emulatorFile = null;

    // Result message: 'creado' | 'modificado'
    private String result;

    // Sets the emulator to edit
    public void init(Emulator emulator) {
        if (emulator != null) {
            this.emulator = emulator;
            this.oldEmulator = new Emulator(emulator);
            lblTitle.setText("Edición de emulador");
            txtNameEmulator.setText(emulator.getName());
            lblPathEmulator.setText(emulator.getPath());
            result = "modificado";
        } else {
            this.emulator = new Emulator();
            lblTitle.setText("Registro de nuevo emulador");
            result = "creado";
        }
    }

    @FXML
    private void onClickBtnSelectPathEmulator(ActionEvent event) {
        Stage stage = (Stage) btnSelectPathEmulator.getScene().getWindow();
        Properties properties = PropertiesConfig.readProperties();
        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle("Seleccionar emulador");
        // Sets initial directory that it is save in the config file, if not setted, the initial
        // directory will be user home
        if (properties != null) {
            File initDir = new File(properties.getProperty(PropertiesConfig.EMULATOR_LAST_PATH,
                    System.getProperty("user.home")));
            if (initDir.exists())
                fileChooser.setInitialDirectory(initDir);
            else
                fileChooser.setInitialDirectory(new File(System.getProperty("user.home")));

        }
        emulatorFile = fileChooser.showOpenDialog(stage);
        if (emulatorFile != null) {
            lblPathEmulator.setText(emulatorFile.getAbsolutePath());
            // Saves the last directory into the config file
            PropertiesConfig.writeEmulatorLastPathProperites(emulatorFile.getParent());
        }
    }

    @FXML
    private void onClickBtnCancel(ActionEvent event) {
        Stage stage = (Stage) btnCancel.getScene().getWindow();
        stage.close();
    }

    @FXML
    private void onClickBtnSave(ActionEvent event) {
        String name = txtNameEmulator.getText();
        String path = lblPathEmulator.getText();

        if (!name.isEmpty() && !path.isEmpty()) {
            EmulatorService emulatorService = new JDBCEmulatorService();
            try {
                this.emulator.setName(name);
                this.emulator.setPath(path);
                // If emulator has been edited
                if (oldEmulator == null || !oldEmulator.equals(emulator)) {
                    if (oldEmulator == null)
                        emulatorService.insert(emulator);
                    else
                        emulatorService.modify(emulator);
                    AppDialog.messageDialog("Edición de emulador",
                            "Emulador " + name + " " + result + " con éxito.");
                    Stage stage = (Stage) btnSave.getScene().getWindow();
                    stage.close();
                }
            } catch (CrudException e) {
                AppDialog.errorDialog(e.getMessage(), e.getCause().toString());
            }
        } else {
            AppDialog.errorDialog("Error en la edición de emuladores",
                    "Debes rellenar los campos obligatorios.");
        }

    }
}
