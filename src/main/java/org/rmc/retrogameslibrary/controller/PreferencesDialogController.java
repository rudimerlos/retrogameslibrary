/***************************************************************************************************
 * Copyright (C) 2021 Rudi Merlos Carrión
 *
 * This file is part of RetroGamesLibrary.
 *
 * RetroGamesLibrary is free software: you can redistribute it and/or modify it under the terms of
 * the GNU General Public License as published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version.
 *
 * RetroGamesLibrary is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with RetroGamesLibrary.
 * If not, see <https://www.gnu.org/licenses/>.
 **************************************************************************************************/

package org.rmc.retrogameslibrary.controller;

import java.util.Properties;
import org.rmc.retrogameslibrary.config.PropertiesConfig;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.stage.Stage;

public class PreferencesDialogController {

    @FXML
    private CheckBox chkInitHelp;
    @FXML
    private CheckBox chkEnableTooltips;

    @FXML
    private Button btnOk;
    @FXML
    private Button btnCancel;

    private MainController mainWindow = null;

    @FXML
    public void initialize() {
        // Set check boxes status from properties file
        Properties properties = PropertiesConfig.readProperties();
        if (properties != null) {
            chkInitHelp
                    .setSelected(properties.getProperty(PropertiesConfig.INIT_HELP).equals("true"));
            chkEnableTooltips
                    .setSelected(properties.getProperty(PropertiesConfig.TOOLTIPS).equals("true"));
        }
    }

    public void init(MainController mainWindow) {
        this.mainWindow = mainWindow;
    }

    @FXML
    private void onClickBtnOk(ActionEvent event) {
        PropertiesConfig.writeInitHelpProperties(chkInitHelp.isSelected());
        PropertiesConfig.writeTooltipProperties(chkEnableTooltips.isSelected());
        // Enable or disable tooltips in main window in real time
        this.mainWindow.setTooltips(chkEnableTooltips.isSelected());
        Stage stage = (Stage) btnOk.getScene().getWindow();
        stage.close();
    }

    @FXML
    private void onClickBtnCancel(ActionEvent envent) {
        Stage stage = (Stage) btnCancel.getScene().getWindow();
        stage.close();
    }
}
