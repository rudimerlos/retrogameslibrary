/***************************************************************************************************
 * Copyright (C) 2021 Rudi Merlos Carrión
 *
 * This file is part of RetroGamesLibrary.
 *
 * RetroGamesLibrary is free software: you can redistribute it and/or modify it under the terms of
 * the GNU General Public License as published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version.
 *
 * RetroGamesLibrary is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with RetroGamesLibrary.
 * If not, see <https://www.gnu.org/licenses/>.
 **************************************************************************************************/

package org.rmc.retrogameslibrary.controller;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.Type;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import com.google.gson.reflect.TypeToken;
import org.rmc.retrogameslibrary.config.PropertiesConfig;
import org.rmc.retrogameslibrary.dialog.AppDialog;
import org.rmc.retrogameslibrary.model.Emulator;
import org.rmc.retrogameslibrary.model.Game;
import org.rmc.retrogameslibrary.model.Platform;
import org.rmc.retrogameslibrary.repository.CrudException;
import org.rmc.retrogameslibrary.service.EmulatorService;
import org.rmc.retrogameslibrary.service.GameService;
import org.rmc.retrogameslibrary.service.PlatformService;
import org.rmc.retrogameslibrary.service.jdbc.JDBCConnection;
import org.rmc.retrogameslibrary.service.jdbc.JDBCEmulatorService;
import org.rmc.retrogameslibrary.service.jdbc.JDBCGameService;
import org.rmc.retrogameslibrary.service.jdbc.JDBCPlatformService;
import org.rmc.retrogameslibrary.util.DirUtility;
import org.rmc.retrogameslibrary.util.JsonUtitlity;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Bounds;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.ContextMenu;
import javafx.scene.control.Label;
import javafx.scene.control.MenuItem;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableRow;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.Tooltip;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyCodeCombination;
import javafx.scene.input.KeyCombination;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.stage.DirectoryChooser;
import javafx.stage.FileChooser;
import javafx.stage.FileChooser.ExtensionFilter;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.util.Duration;

public class MainController {

    @FXML
    private MenuItem menuAddNewGame;
    @FXML
    private MenuItem menuImport;
    @FXML
    private MenuItem menuExport;
    @FXML
    private MenuItem menuQuit;
    @FXML
    private MenuItem menuPlayGame;
    @FXML
    private MenuItem menuEditGame;
    @FXML
    private MenuItem menuDelete;
    @FXML
    private MenuItem menuPlatforms;
    @FXML
    private MenuItem menuEmulators;
    @FXML
    private MenuItem menuPreferences;
    @FXML
    private MenuItem menuAbout;

    @FXML
    private Button btnAddNewGame;
    @FXML
    private Button btnPlayGame;
    @FXML
    private Label lblTotal;
    @FXML
    private ComboBox<String> cmbSelectPlatform;
    @FXML
    private TextField txtSearch;

    @FXML
    private TableView<Game> tableGames;
    @FXML
    private TableColumn<Game, String> colTitleGame;
    @FXML
    private TableColumn<Game, String> colPlatformGame;
    @FXML
    private TableColumn<Game, Integer> colYearGame;
    @FXML
    private TableColumn<Game, String> colGenderGame;
    @FXML
    private TableColumn<Game, String> colEmulator;
    @FXML
    private TableColumn<Game, ImageView> colRom;
    @FXML
    private TableColumn<Game, ImageView> colScreenshot;

    @FXML
    private AnchorPane paneDetails;
    @FXML
    private ImageView imgPhotoDetails;
    @FXML
    private Label lblTitleDetails;
    @FXML
    private Label lblPlatformDetails;
    @FXML
    private Label lblGenderDetails;
    @FXML
    private Label lblYearDetails;
    @FXML
    private Label lblDescriptionDetails;
    @FXML
    private Button btnPlayGameDetails;
    @FXML
    private Button btnEditGame;
    @FXML
    private Button btnDelete;

    @FXML
    private Tooltip tipAddNewGame;
    @FXML
    private Tooltip tipPlayGame;
    @FXML
    private Tooltip tipSearch;
    @FXML
    private Tooltip tipPlayGameDetails;
    @FXML
    private Tooltip tipEditGameDetails;
    @FXML
    private Tooltip tipDeleteGameDetails;

    private ContextMenu contextMenu;

    private ObjectProperty<TableRow<Game>> lastSelectedRow;

    private GameService gameService;
    private EmulatorService emulatorService;
    private PlatformService platformService;

    private String selection; // Set selected platforms to show

    @FXML
    public void initialize() throws IOException {
        gameService = new JDBCGameService();
        emulatorService = new JDBCEmulatorService();
        platformService = new JDBCPlatformService();

        cmbSelectPlatform.setItems(getPlatformList());
        cmbSelectPlatform.getSelectionModel().selectFirst();

        selection = cmbSelectPlatform.getSelectionModel().getSelectedItem();

        tableGames.setPlaceholder(new Label("No hay juegos disponibles."));
        tableGames.setFixedCellSize(60);
        showGames(getGameList());

        // Store the last row selected
        lastSelectedRow = new SimpleObjectProperty<>();
        tableGames.setRowFactory(table -> {
            TableRow<Game> row = new TableRow<>();
            row.selectedProperty().addListener((observable, oldValue, newValue) -> {
                if (newValue)
                    lastSelectedRow.set(row);
            });
            return row;
        });

        createContextMenu();

        // Event listener to show game details when it is selected
        tableGames.getSelectionModel().selectedItemProperty()
                .addListener((observable, oldValue, newValue) -> {
                    if (newValue != null) {
                        tableGames.setContextMenu(contextMenu);
                        // newValue is the game selected
                        try {
                            showDetails(newValue);
                        } catch (CrudException e) {
                            e.printStackTrace();
                        }
                        // When a game is selected, enables edit and delete buttons
                        paneDetails.setMinWidth(300);
                        paneDetails.setMaxWidth(300);
                        menuPlayGame.setDisable(true);
                        menuEditGame.setDisable(false);
                        menuDelete.setDisable(false);
                        btnEditGame.setVisible(true);
                        btnDelete.setVisible(true);
                        btnPlayGame.setDisable(true);
                        btnPlayGameDetails.setVisible(true);
                        contextMenu.getItems().get(0).setDisable(true);
                        try {
                            // If game selected has an emulator and rom setted, play button will be
                            // enabled
                            if (newValue.getEmulator_idId() != null
                                    && newValue.getStringPath() != null
                                    && Files.exists(Paths.get(PropertiesConfig.ROOT_PATH,
                                            newValue.getStringPath()))) {
                                menuPlayGame.setDisable(false);
                                btnPlayGame.setDisable(false);
                                btnPlayGameDetails.setDisable(false);
                                contextMenu.getItems().get(0).setDisable(false);
                            } else {
                                btnPlayGameDetails.setDisable(true);
                            }
                        } catch (NullPointerException e) {
                            // This try-catch is here to solve an issue of JavaFX library but it
                            // does not affect the operation of the program
                        }
                    } else {
                        paneDetails.setMinWidth(0);
                        paneDetails.setMaxWidth(0);
                        tableGames.setContextMenu(null);
                        menuPlayGame.setDisable(true);
                        menuEditGame.setDisable(true);
                        menuDelete.setDisable(true);
                        btnPlayGame.setDisable(true);
                        resetDetails();
                    }
                });

        // Event listener to search text
        txtSearch.textProperty().addListener((observable, oldValue, newValue) -> {
            try {
                ObservableList<Game> gameList = FXCollections.observableArrayList();
                if (!selection.equals("All platforms")) {
                    String[] splitPlatform = selection.split("-");
                    String name = splitPlatform[0].trim();
                    String model = splitPlatform[1].trim();
                    Platform platform = platformService.getByNameAndModel(name, model);
                    gameList.setAll(gameService.searchByTitleAndPlatformId("%" + newValue + "%",
                            platform.getId()));
                } else {
                    gameList.setAll(gameService.searchByTitle("%" + newValue + "%"));
                }
                showGames(gameList);
            } catch (CrudException e) {
                AppDialog.errorDialog(e.getMessage(), e.getCause().toString());
            }
        });

        // Set tooltips visibility
        String propertiesTooltip =
                PropertiesConfig.readProperties().getProperty(PropertiesConfig.TOOLTIPS, "true");
        setTooltips(propertiesTooltip.equals("true"));
    }

    // Read platforms from database and they set into ObservableList
    private ObservableList<String> getPlatformList() {
        ObservableList<String> platformList = FXCollections.observableArrayList();
        try {
            List<Platform> platforms = platformService.getAll();
            platformList.add("All platforms");
            platforms.forEach(platform -> {
                platformList.add(platform.getName() + " - " + platform.getModel());
            });
        } catch (CrudException e) {
            AppDialog.errorDialog(e.getMessage(), e.getCause().toString());
        }
        return platformList;
    }

    // Set tooltips duration and visibility
    public void setTooltips(boolean active) {
        tipAddNewGame.setShowDelay(Duration.millis(100));
        tipPlayGame.setShowDelay(Duration.millis(100));
        tipSearch.setShowDelay(Duration.millis(100));
        tipPlayGameDetails.setShowDelay(Duration.millis(100));
        tipEditGameDetails.setShowDelay(Duration.millis(100));
        tipDeleteGameDetails.setShowDelay(Duration.millis(100));
        if (active) {
            btnAddNewGame.setTooltip(tipAddNewGame);
            btnPlayGame.setTooltip(tipPlayGame);
            txtSearch.setTooltip(tipSearch);
            btnPlayGameDetails.setTooltip(tipPlayGameDetails);
            btnEditGame.setTooltip(tipEditGameDetails);
            btnDelete.setTooltip(tipDeleteGameDetails);
        } else {
            btnAddNewGame.setTooltip(null);
            btnPlayGame.setTooltip(null);
            txtSearch.setTooltip(null);
            btnPlayGameDetails.setTooltip(null);
            btnEditGame.setTooltip(null);
            btnDelete.setTooltip(null);
        }
    }

    // Create a context menu with the same options as the edit menu
    private void createContextMenu() {
        MenuItem contextPlayGame = new MenuItem("Jugar");
        contextPlayGame
                .setAccelerator(new KeyCodeCombination(KeyCode.P, KeyCombination.CONTROL_DOWN));
        contextPlayGame.setOnAction(e -> launchEmulator());
        MenuItem contextEditGame = new MenuItem("Editar");
        contextEditGame
                .setAccelerator(new KeyCodeCombination(KeyCode.E, KeyCombination.CONTROL_DOWN));
        contextEditGame.setOnAction(e -> editGame());
        MenuItem contextDeleteGame = new MenuItem("Eliminar");
        contextDeleteGame.setAccelerator(new KeyCodeCombination(KeyCode.DELETE));
        contextDeleteGame.setOnAction(e -> deleteGame());

        contextMenu = new ContextMenu();
        contextMenu.getItems().addAll(contextPlayGame, contextEditGame, contextDeleteGame);
    }

    // Read games from database and they set into ObservableList
    private ObservableList<Game> getGameList() {
        ObservableList<Game> gameList = FXCollections.observableArrayList();
        try {
            if (selection.equals("All platforms")) {
                List<Game> games = gameService.getAll();
                if (games != null)
                    gameList.setAll(games);
            } else {
                String[] splitPlatform = selection.split("-");
                String name = splitPlatform[0].trim();
                String model = splitPlatform[1].trim();
                Platform platform = platformService.getByNameAndModel(name, model);
                List<Game> games = gameService.getByPlatform(platform);
                if (games != null)
                    gameList.setAll(games);
            }
        } catch (CrudException e) {
            AppDialog.errorDialog(e.getMessage(), e.getCause().toString());
        }
        lblTotal.setText(String.valueOf(gameList.size()));
        return gameList;
    }

    // Show games in the games table
    private void showGames(ObservableList<Game> games) {
        colTitleGame.setCellValueFactory(new PropertyValueFactory<Game, String>("title"));
        colPlatformGame.setCellValueFactory(new PropertyValueFactory<Game, String>("platform_id"));
        colYearGame.setCellValueFactory(new PropertyValueFactory<Game, Integer>("year"));
        colGenderGame.setCellValueFactory(new PropertyValueFactory<Game, String>("gender"));
        colEmulator.setCellValueFactory(new PropertyValueFactory<Game, String>("emulator_id"));
        colRom.setCellValueFactory(new PropertyValueFactory<Game, ImageView>("path"));
        colScreenshot.setCellValueFactory(new PropertyValueFactory<Game, ImageView>("screenshot"));

        tableGames.setItems(games);
        tableGames.refresh();
    }

    // Show game details in the right panel
    private void showDetails(Game game) throws CrudException {
        resetDetails();
        if (game != null) {
            try {
                imgPhotoDetails.setImage(game.getScreenshot().getImage());
                imgPhotoDetails.setPreserveRatio(true);
                imgPhotoDetails.setFitHeight(180);
            } catch (Exception e) {
                imgPhotoDetails.setImage(null);
            }
            lblTitleDetails.setText(game.getTitle());
            lblPlatformDetails.setText(game.getPlatform_id());
            lblGenderDetails.setText(game.getGender());
            lblYearDetails.setText(game.getYear() == null ? "" : game.getYear().toString());
            lblDescriptionDetails.setText(game.getDescription());
        }
    }

    // Reset details in the right panel
    private void resetDetails() {
        imgPhotoDetails.setImage(null);
        lblTitleDetails.setText("");
        lblPlatformDetails.setText("");
        lblGenderDetails.setText("");
        lblYearDetails.setText("");
        lblDescriptionDetails.setText("");
        btnEditGame.setVisible(false);
        btnDelete.setVisible(false);
        btnPlayGameDetails.setVisible(false);
    }

    @FXML
    private void onClickAddNewGame(ActionEvent event) throws IOException {
        Stage stage = (Stage) btnAddNewGame.getScene().getWindow();
        gameEditWindow(stage, null).setOnHidden(e -> {
            showGames(getGameList());
            txtSearch.setText("");
        });
    }

    @FXML
    private void onClickImport(ActionEvent event) {
        AppDialog.warningDialog("Aviso importante",
                "Esta acción importará toda la información de una colección de juegos, plataformas "
                        + "y emuladores.");
        Stage stage = (Stage) btnAddNewGame.getScene().getWindow();
        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle("Importar colección");
        fileChooser.setInitialDirectory(new File(System.getProperty("user.home")));
        // Only the rgldata.zip file will be visible
        fileChooser.getExtensionFilters().add(new ExtensionFilter("Zip Files", "rgldata.zip"));
        File source = fileChooser.showOpenDialog(stage);
        if (source != null) {
            if (AppDialog.confirmationDialog("Importar",
                    "Esta acción sobreescribirá la actual colección.",
                    "¿Estás seguro de que quieres continuar?")) {
                try {
                    // Delete database file and resources
                    platformService.removeAll();
                    emulatorService.removeAll();
                    if (Files.exists(Paths.get(DirUtility.PATH_TO_ROMS)))
                        DirUtility.deleteDir(DirUtility.PATH_TO_ROMS);
                    // Import database file and resources
                    DirUtility.decompressZIP(source.toString(), PropertiesConfig.ROOT_PATH);
                    // Read JSON files and fill database
                    if (Files.exists(Paths.get(PropertiesConfig.ROOT_PATH, "emulators.json"))) {
                        Type emulatorType = new TypeToken<List<Emulator>>() {}.getType();
                        List<Emulator> emulatorList = JsonUtitlity.readJson(
                                Paths.get(PropertiesConfig.ROOT_PATH, "emulators.json"),
                                emulatorType);
                        for (Emulator emulator : emulatorList)
                            emulatorService.insertWithId(emulator);
                    }
                    if (Files.exists(Paths.get(PropertiesConfig.ROOT_PATH, "platforms.json"))) {
                        Type platfomType = new TypeToken<List<Platform>>() {}.getType();
                        List<Platform> platformList = JsonUtitlity.readJson(
                                Paths.get(PropertiesConfig.ROOT_PATH, "platforms.json"),
                                platfomType);
                        for (Platform platform : platformList)
                            platformService.insertWithId(platform);
                    }
                    if (Files.exists(Paths.get(PropertiesConfig.ROOT_PATH, "games.json"))) {
                        Type gameType = new TypeToken<List<Game>>() {}.getType();
                        List<Game> gameList = JsonUtitlity.readJson(
                                Paths.get(PropertiesConfig.ROOT_PATH, "games.json"), gameType);
                        for (Game game : gameList)
                            gameService.insertWithId(game);
                    }
                    // Delete JSON files
                    JsonUtitlity.deleteJsonFiles(
                            PropertiesConfig.ROOT_PATH + DirUtility.SEPARATOR + "emulators.json",
                            PropertiesConfig.ROOT_PATH + DirUtility.SEPARATOR + "platforms.json",
                            PropertiesConfig.ROOT_PATH + DirUtility.SEPARATOR + "games.json");
                    // Show updated game collection
                    showGames(getGameList());
                } catch (IOException | CrudException e) {
                    AppDialog.errorDialog("Error",
                            "Se ha producido un error al importar la colección.",
                            e.getCause() == null ? e.getMessage() : e.getCause().toString());
                }
            }
        }
    }

    @FXML
    private void onClickExport(ActionEvent event) {
        Stage stage = (Stage) btnAddNewGame.getScene().getWindow();
        DirectoryChooser directoryChooser = new DirectoryChooser();
        directoryChooser.setTitle("Exportar colección");
        directoryChooser.setInitialDirectory(new File(System.getProperty("user.home")));
        File dest = directoryChooser.showDialog(stage);
        if (dest != null) {
            try {
                // Write JSON files
                List<String> jsonFiles = new ArrayList<>();
                List<Emulator> listEmulators = emulatorService.getAll();
                if (listEmulators != null && !listEmulators.isEmpty()) {
                    JsonUtitlity.writeJson(Paths.get("emulators.json"), listEmulators);
                    jsonFiles.add("emulators.json");
                }
                List<Platform> listPlatforms = platformService.getAll();
                if (listPlatforms != null && !listPlatforms.isEmpty()) {
                    JsonUtitlity.writeJson(Paths.get("platforms.json"), listPlatforms);
                    jsonFiles.add("platforms.json");
                }
                List<Game> listGames = gameService.getAll();
                if (listGames != null && !listGames.isEmpty()) {
                    JsonUtitlity.writeJson(Paths.get("games.json"), listGames);
                    jsonFiles.add("games.json");
                }
                Path pathToAdd = Paths.get(PropertiesConfig.ROOT_PATH, DirUtility.PATH_TO_ROMS);
                jsonFiles.add(Files.exists(pathToAdd) ? pathToAdd.toString() : null);
                // Export database file and resources
                DirUtility.compressZIP(dest.toString() + DirUtility.SEPARATOR + "rgldata.zip",
                        jsonFiles);
                // Delete JSON files
                JsonUtitlity.deleteJsonFiles("emulators.json", "platforms.json", "games.json");
                AppDialog.messageDialog("Exportar", "La colección se ha exportado correctamente.");
            } catch (IOException | CrudException e) {
                AppDialog.errorDialog("Error", "Se ha producido un error al exportar la colección.",
                        e.getCause() == null ? e.getMessage() : e.getCause().toString());
            }
        }
    }

    @FXML
    private void onClickQuit(ActionEvent event) throws CrudException {
        if (AppDialog.confirmationDialog("Salir de Retro Games Library",
                "¿Estás seguro de que quieres salir de la aplicación?"))
            JDBCConnection.getInstance().closeConnection();
        javafx.application.Platform.exit();
    }

    @FXML
    private void onClickEditGame(ActionEvent event) {
        editGame();
    }

    @FXML
    private void showSelectedPlatform(ActionEvent event) {
        txtSearch.setText("");
        selection = cmbSelectPlatform.getSelectionModel().getSelectedItem();
        if (selection != null)
            showGames(getGameList());
    }

    @FXML
    private void onMouseClickedCol(MouseEvent event) {
        // If empty row is selected, clear selection
        if (lastSelectedRow.get() != null) {
            Bounds bounds =
                    lastSelectedRow.get().localToScene(lastSelectedRow.get().getLayoutBounds());
            if (bounds.contains(event.getSceneX(), event.getSceneY()) == false)
                tableGames.getSelectionModel().clearSelection();
        }
        // If double click in a selection, edit game
        if (event.getButton() == MouseButton.PRIMARY && event.getClickCount() > 1) {
            Game game = tableGames.getSelectionModel().getSelectedItem();
            if (game != null && game.getEmulator_id() != null && game.getStringPath() != null
                    && Files.exists(Paths.get(PropertiesConfig.ROOT_PATH, game.getStringPath())))
                launchEmulator();
            else
                editGame();
        }
    }

    @FXML
    private void onKeyReleasedCol(KeyEvent event) {
        if (event.getCode() == KeyCode.ENTER) {
            Game game = tableGames.getSelectionModel().getSelectedItem();
            if (game != null && game.getEmulator_id() != null && game.getStringPath() != null
                    && Files.exists(Paths.get(PropertiesConfig.ROOT_PATH, game.getStringPath())))
                launchEmulator();
            else
                editGame();
        }
    }

    // Show game edit window with the selected game and show games updated when is finished
    private void editGame() {
        Game game = tableGames.getSelectionModel().getSelectedItem();
        if (game != null) {
            Stage stage = (Stage) btnAddNewGame.getScene().getWindow();
            try {
                gameEditWindow(stage, game).setOnHidden(e -> {
                    showGames(getGameList());
                    txtSearch.setText("");
                });
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    @FXML
    private void onClickDeleteGame(ActionEvent event) {
        deleteGame();
    }

    // Delete game from database and resources
    private void deleteGame() {
        Game game = tableGames.getSelectionModel().getSelectedItem();
        if (game != null) {
            if (AppDialog.confirmationDialog("Eliminar juegos",
                    "¿Estás seguro de que quieres borrar el juego " + game.getTitle() + "?")) {
                try {
                    // Delete resources dir if exists
                    if (Files.exists(Paths.get(PropertiesConfig.ROOT_PATH,
                            DirUtility.getGameDirName(game)))) {
                        DirUtility.deleteDir(Paths
                                .get(PropertiesConfig.ROOT_PATH, DirUtility.getGameDirName(game))
                                .toString());
                    }
                    gameService.remove(game);
                    AppDialog.messageDialog("Eliminar juegos",
                            "Se ha eliminado el juego " + game.getTitle() + " con éxito.");
                    tableGames.getSelectionModel().clearSelection();
                    resetDetails();
                    showGames(getGameList());
                    txtSearch.setText("");
                } catch (CrudException e) {
                    AppDialog.errorDialog(e.getMessage(), e.getCause().toString());
                } catch (IOException e) {
                    AppDialog.errorDialog("Error",
                            "Se ha producido un error al eliminar el directorio "
                                    + "de recursos del juego.");
                }
            }
        }
    }

    @FXML
    private void onClickPlatforms(ActionEvent event) throws IOException {
        Stage stage = (Stage) btnAddNewGame.getScene().getWindow();
        platformWindow(stage).setOnHidden(e -> {
            cmbSelectPlatform.setItems(getPlatformList());
            cmbSelectPlatform.getSelectionModel().selectFirst();
            selection = cmbSelectPlatform.getSelectionModel().getSelectedItem();
            showGames(getGameList());
        });
    }

    // Show platforms window
    private Stage platformWindow(Stage stage) throws IOException {
        Stage newStage = new Stage();
        newStage.initOwner(stage);
        newStage.initModality(Modality.WINDOW_MODAL);
        Parent root = FXMLLoader.load(getClass().getResource("/view/platformdialog.fxml"));
        newStage.setScene(new Scene(root));
        newStage.setTitle("Platforms");
        newStage.setResizable(false);
        newStage.show();
        return newStage;
    }

    @FXML
    private void onClickUsers(ActionEvent event) throws IOException {
        Stage thisStage = (Stage) btnAddNewGame.getScene().getWindow();
        Stage newStage = new Stage();
        newStage.initOwner(thisStage);
        newStage.initModality(Modality.WINDOW_MODAL);
        Parent root = FXMLLoader.load(getClass().getResource("/view/usersdialog.fxml"));
        newStage.setScene(new Scene(root));
        newStage.setTitle("Users");
        newStage.setResizable(false);
        newStage.show();
    }

    @FXML
    private void onClickEmulators(ActionEvent event) throws IOException {
        Stage stage = (Stage) btnAddNewGame.getScene().getWindow();
        emulatorWindow(stage).setOnHidden(e -> showGames(getGameList()));
    }

    // Show emulators window
    private Stage emulatorWindow(Stage stage) throws IOException {
        Stage newStage = new Stage();
        newStage.initOwner(stage);
        newStage.initModality(Modality.WINDOW_MODAL);
        Parent root = FXMLLoader.load(getClass().getResource("/view/emulatordialog.fxml"));
        newStage.setScene(new Scene(root));
        newStage.setTitle("Emulators");
        newStage.setResizable(false);
        newStage.show();
        return newStage;
    }

    @FXML
    private void onClickPreferences(ActionEvent event) throws IOException {
        Stage stage = (Stage) btnAddNewGame.getScene().getWindow();
        Stage newStage = new Stage();
        newStage.initOwner(stage);
        newStage.initModality(Modality.WINDOW_MODAL);
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/view/preferencesdialog.fxml"));
        Parent root = loader.load();
        PreferencesDialogController controller = loader.getController();
        controller.init(this); // Pass this class to access to setTooltips method
        newStage.setScene(new Scene(root));
        newStage.setTitle("Preferencias");
        newStage.setResizable(false);
        newStage.show();
    }

    @FXML
    private void onClickAbout(ActionEvent event) throws IOException {
        Stage stage = (Stage) btnAddNewGame.getScene().getWindow();
        Stage newStage = new Stage();
        newStage.initOwner(stage);
        newStage.initModality(Modality.WINDOW_MODAL);
        Parent root = FXMLLoader.load(getClass().getResource("/view/aboutwindow.fxml"));
        newStage.setScene(new Scene(root));
        newStage.setTitle("Acerca de Retro Games Library");
        newStage.setResizable(false);
        newStage.show();
    }

    @FXML
    private void onClickPlayGame(ActionEvent event) {
        launchEmulator();
    }

    private void launchEmulator() {
        // Get the selected game in the table view
        Game game = tableGames.getSelectionModel().getSelectedItem();
        // Create list to store all command parameters
        List<String> command = new ArrayList<>();
        // Executable
        try {
            Emulator emulator = emulatorService.getById(game.getEmulator_idId());
            command.add(emulator.getPath());
        } catch (CrudException e) {
            e.printStackTrace();
        }
        // Pre args
        if (game.getPreArgs() != null && !game.getPreArgs().isEmpty()) {
            String[] preArgs = game.getPreArgs().split(" ");
            for (String s : preArgs)
                command.add(s);
        }
        // File to execute
        command.add(PropertiesConfig.ROOT_PATH + DirUtility.SEPARATOR + game.getStringPath());
        // Post args
        if (game.getPostArgs() != null && !game.getPostArgs().isEmpty()) {
            String[] postArgs = game.getPostArgs().split(" ");
            for (String s : postArgs)
                command.add(s);
        }
        // Convert list to string array
        String[] cmdArray = new String[command.size()];
        for (int i = 0; i < cmdArray.length; i++)
            cmdArray[i] = command.get(i);
        // Execute command
        try {
            Runtime.getRuntime().exec(cmdArray);
        } catch (IOException e) {
            AppDialog.errorDialog("Error", "Error al ejecutar el emulador.",
                    "Ha ocurrido un error desconocido al intentar ejecutar el emulador. Por favor, "
                            + "revise la configuración.");
        }
    }

    // Show game edit window
    private Stage gameEditWindow(Stage stage, Game game) throws IOException {
        Stage newStage = new Stage();
        newStage.initOwner(stage);
        newStage.initModality(Modality.WINDOW_MODAL);
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/view/gameeditdialog.fxml"));
        Parent root = loader.load();
        GameEditDialogController controller = loader.getController();
        controller.init(game);
        newStage.setScene(new Scene(root));
        newStage.setTitle(game == null ? "Registro de juegos" : "Edición de juegos");
        newStage.setResizable(false);
        newStage.show();
        return newStage;
    }
}
