/***************************************************************************************************
 * Copyright (C) 2021 Rudi Merlos Carrión
 *
 * This file is part of RetroGamesLibrary.
 *
 * RetroGamesLibrary is free software: you can redistribute it and/or modify it under the terms of
 * the GNU General Public License as published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version.
 *
 * RetroGamesLibrary is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with RetroGamesLibrary.
 * If not, see <https://www.gnu.org/licenses/>.
 **************************************************************************************************/

package org.rmc.retrogameslibrary.controller;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;
import java.util.Properties;
import org.rmc.retrogameslibrary.config.PropertiesConfig;
import org.rmc.retrogameslibrary.dialog.AppDialog;
import org.rmc.retrogameslibrary.model.Emulator;
import org.rmc.retrogameslibrary.model.Game;
import org.rmc.retrogameslibrary.model.Platform;
import org.rmc.retrogameslibrary.repository.CrudException;
import org.rmc.retrogameslibrary.service.EmulatorService;
import org.rmc.retrogameslibrary.service.GameService;
import org.rmc.retrogameslibrary.service.PlatformService;
import org.rmc.retrogameslibrary.service.jdbc.JDBCEmulatorService;
import org.rmc.retrogameslibrary.service.jdbc.JDBCGameService;
import org.rmc.retrogameslibrary.service.jdbc.JDBCPlatformService;
import org.rmc.retrogameslibrary.util.DirUtility;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.control.Tooltip;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.stage.FileChooser;
import javafx.stage.FileChooser.ExtensionFilter;
import javafx.stage.Stage;

public class GameEditDialogController {

    @FXML
    private Label lblTitle;
    @FXML
    private TextField txtTitleGame;
    @FXML
    private TextArea txaDescriptionGame;
    @FXML
    private TextField txtYearGame;
    @FXML
    private TextField txtGenderGame;
    @FXML
    private ComboBox<String> cmbEmulatorGame;
    @FXML
    private TextField txtPreargsGame;
    @FXML
    private TextField txtPostargsGame;
    @FXML
    private Button btnSelectGame;
    @FXML
    private Label lblPathGame;

    @FXML
    private ComboBox<String> cmbPlatformGame;
    @FXML
    private Button btnSelectImgGame;
    @FXML
    private ImageView imgScreenshotGame;

    @FXML
    private Button btnCancel;
    @FXML
    private Button btnSave;

    private File gameFile = null;
    private boolean gameFileIsChanged;
    private File imgFile = null;
    private boolean imgFileIsChanged;

    private Game game = null; // new game data
    private Game oldGame = null; // game data before change them

    // Result message: 'creado' | 'modificado'
    private String result;

    private GameService gameService;
    private PlatformService platformService;
    private EmulatorService emulatorService;

    @FXML
    public void initialize() {
        gameService = new JDBCGameService();
        platformService = new JDBCPlatformService();
        emulatorService = new JDBCEmulatorService();

        // Initialize combo boxes
        cmbPlatformGame.setItems(getPlatformList());
        cmbEmulatorGame.setItems(getEmulatorList());
        // Create tooltips
        Tooltip tooltip = new Tooltip("Emulador [preargs] juego [postargs]");
        txtPreargsGame.setTooltip(tooltip);
        txtPostargsGame.setTooltip(tooltip);
    }

    // Set the game to edit
    public void init(Game game) {
        if (game != null) {
            this.game = game;
            this.oldGame = new Game(game);
            lblTitle.setText("Edición de juego");
            txtTitleGame.setText(game.getTitle());
            txaDescriptionGame.setText(game.getDescription());
            txtYearGame.setText(game.getYear() == null ? "" : game.getYear().toString());
            txtGenderGame.setText(game.getGender());
            if (game.getEmulator_idId() != null)
                cmbEmulatorGame.getSelectionModel().select(game.getEmulator_id());
            txtPreargsGame.setText(game.getPreArgs());
            txtPostargsGame.setText(game.getPostArgs());
            if (game.getStringPath() != null
                    && Files.exists(Paths.get(PropertiesConfig.ROOT_PATH, game.getStringPath())))
                gameFile = new File(
                        PropertiesConfig.ROOT_PATH + DirUtility.SEPARATOR + game.getStringPath());
            if (gameFile != null)
                lblPathGame.setText(
                        PropertiesConfig.ROOT_PATH + DirUtility.SEPARATOR + game.getStringPath());
            try {
                Platform platform = platformService.getById(game.getPlatform_idId());
                cmbPlatformGame.getSelectionModel().select(platform.toString());
            } catch (CrudException e) {
                e.printStackTrace();
            }
            if (game.getScreenshot() != null && Files
                    .exists(Paths.get(PropertiesConfig.ROOT_PATH, game.getScreenshotPath())))
                imgFile = new File(PropertiesConfig.ROOT_PATH + DirUtility.SEPARATOR
                        + game.getScreenshotPath());
            if (imgFile != null) {
                try {
                    imgScreenshotGame.setImage(new Image(Files.newInputStream(imgFile.toPath())));
                } catch (IOException e) {
                    imgScreenshotGame.setImage(null);
                }
            }
            result = "modificado";
        } else {
            this.game = new Game();
            lblTitle.setText("Registro de nuevo juego");
            result = "creado";
        }
        gameFileIsChanged = imgFileIsChanged = false;
    }

    // Read platforms from database and they set into ObservableList
    private ObservableList<String> getPlatformList() {
        ObservableList<String> platformList = FXCollections.observableArrayList();
        try {
            List<Platform> platforms = platformService.getAll();
            platforms.forEach(platform -> {
                platformList.add(platform.getName() + " - " + platform.getModel());
            });
        } catch (CrudException e) {
            AppDialog.errorDialog(e.getMessage(), e.getCause().toString());
        }
        return platformList;
    }

    // Read emulators from database and they set into ObservableList
    private ObservableList<String> getEmulatorList() {
        ObservableList<String> emulatorList = FXCollections.observableArrayList();
        try {
            List<Emulator> emulators = emulatorService.getAll();
            emulators.forEach(emulator -> {
                emulatorList.add(emulator.getName());
            });
        } catch (CrudException e) {
            AppDialog.errorDialog(e.getMessage(), e.getCause().toString());
        }
        return emulatorList;
    }

    @FXML
    private void onClickBtnSelectGame(ActionEvent event) {
        Stage stage = (Stage) btnSelectGame.getScene().getWindow();
        Properties properties = PropertiesConfig.readProperties();
        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle("Seleccionar juego");
        // Sets initial directory that it is save in the config file, if not setted, the initial
        // directory will be user home
        if (properties != null) {
            File initDir = new File(properties.getProperty(PropertiesConfig.GAME_ROM_LAST_PATH,
                    System.getProperty("user.home")));
            if (initDir.exists())
                fileChooser.setInitialDirectory(initDir);
            else
                fileChooser.setInitialDirectory(new File(System.getProperty("user.home")));
        }
        gameFile = fileChooser.showOpenDialog(stage);
        if (gameFile != null) {
            lblPathGame.setText(gameFile.getAbsolutePath());
            // Saves the last directory into the config file
            PropertiesConfig.writeGameLastPathProperties(gameFile.getParent());
            gameFileIsChanged = true;
        }
    }

    @FXML
    private void onClickBtnSelectImgGame(ActionEvent event) throws IOException {
        Stage stage = (Stage) btnSelectImgGame.getScene().getWindow();
        Properties properties = PropertiesConfig.readProperties();
        FileChooser fileChooser = new FileChooser();
        fileChooser.getExtensionFilters().add(
                new ExtensionFilter("Image Files", "*.png", "*.jpg", "*.jpeg", "*.gif", "*.giff"));
        fileChooser.setTitle("Seleccionar imagen");
        // Sets initial directory that it is save in the config file, if not setted, the initial
        // directory will be user home
        if (properties != null) {
            File initDir = new File(properties.getProperty(PropertiesConfig.GAME_IMG_LAST_PATH,
                    System.getProperty("user.home")));
            if (initDir.exists())
                fileChooser.setInitialDirectory(initDir);
            else
                fileChooser.setInitialDirectory(new File(System.getProperty("user.home")));
        }
        imgFile = fileChooser.showOpenDialog(stage);
        if (imgFile != null) {
            imgScreenshotGame.setImage(new Image(Files.newInputStream(imgFile.toPath())));
            // Saves the last directory into the config file
            PropertiesConfig.writeImgLastPathProperites(imgFile.getParent());
            imgFileIsChanged = true;
        }
    }

    @FXML
    private void onClickBtnCancel(ActionEvent event) {
        Stage stage = (Stage) btnCancel.getScene().getWindow();
        stage.close();
    }

    @FXML
    private void onClickBtnSave(ActionEvent event) {
        String title = txtTitleGame.getText();
        String description = txaDescriptionGame.getText();
        String strYear = txtYearGame.getText();
        String gender = txtGenderGame.getText();
        String strEmulator = cmbEmulatorGame.getSelectionModel().getSelectedItem();
        String preArgs = txtPreargsGame.getText();
        String postArgs = txtPostargsGame.getText();
        String path = gameFile != null ? gameFile.getAbsolutePath() : null;
        String strPlatform = cmbPlatformGame.getSelectionModel().getSelectedItem();
        String screenshot = imgFile != null ? imgFile.getAbsolutePath() : null;

        if (strPlatform != null && !title.isEmpty() && !strPlatform.isEmpty()) {
            if (!strYear.isEmpty() && !yearIsValid(strYear)) {
                AppDialog.errorDialog("Error en el año", "Debes de introducir un año válido.");
            } else {
                Short year = strYear.isEmpty() ? null : Short.parseShort(strYear);
                String[] splitPlatform = strPlatform.split("-");
                String name = splitPlatform[0].trim();
                String model = splitPlatform[1].trim();
                try {
                    // Get the platform by name and model
                    Platform platform = platformService.getByNameAndModel(name, model);
                    // Get the emulator by name
                    Emulator emulator = null;
                    if (strEmulator != null && !strEmulator.isEmpty())
                        emulator = emulatorService.getByName(strEmulator);
                    game.setTitle(title);
                    // If platform has been changed, create a copy of game to change resources dir
                    Game aux = null;
                    Platform platformTemp =
                            oldGame != null ? platformService.getById(game.getPlatform_idId())
                                    : null;
                    if (oldGame != null && platformTemp != null && !platform.equals(platformTemp)
                            && DirUtility.existsGameDir(game)) {
                        aux = new Game(game);
                        imgFileIsChanged = screenshot != null;
                        gameFileIsChanged = path != null;
                    }
                    game.setPlatform_id(platform.getId());
                    // If title has been changed, move resources and set members
                    if (oldGame != null && !oldGame.getTitle().equals(game.getTitle()))
                        moveResources();
                    game.setDescription(description);
                    game.setYear(year);
                    game.setGender(gender);
                    if (emulator != null)
                        game.setEmulator_id(emulator.getId());
                    game.setPreArgs(preArgs);
                    game.setPostArgs(postArgs);
                    // If game has been edited
                    if (oldGame == null || !oldGame.equals(game)) {
                        if (oldGame == null)
                            game.setId(gameService.insert(game));
                        else
                            gameService.modify(game);
                        // Save resources
                        if (imgFileIsChanged || gameFileIsChanged) {
                            saveResources(screenshot, path);
                            if (oldGame != null && aux != null)
                                DirUtility.deleteDir(PropertiesConfig.ROOT_PATH
                                        + DirUtility.SEPARATOR + DirUtility.getGameDirName(aux));
                            gameService.modify(game);
                        }
                        AppDialog.messageDialog("Edición de juego",
                                "Juego " + title + " " + result + " con éxito.");
                        Stage stage = (Stage) btnSave.getScene().getWindow();
                        stage.close();
                    }
                } catch (CrudException e) {
                    AppDialog.errorDialog(e.getMessage(), e.getCause().toString());
                } catch (IOException e) {
                    AppDialog.errorDialog("Error", "Se ha producido un error al crear el directorio"
                            + " de recursos del juego.");
                }
            }
        } else {
            AppDialog.errorDialog("Error en la edición de juegos",
                    "Debes rellenar los campos obligatorios.");
        }
    }

    // Save resources into appropriate dir
    private void saveResources(String screenshot, String path) throws IOException {
        if (screenshot != null && imgFileIsChanged) {
            // Create dir if not exists
            if (!DirUtility.existsGameDir(game))
                Files.createDirectories(
                        Paths.get(PropertiesConfig.ROOT_PATH, DirUtility.getGameDirName(game)));
            String screenshotPath = DirUtility.getGameDirName(game) + "cover";
            // Delete old image if exists
            Files.deleteIfExists(Paths.get(PropertiesConfig.ROOT_PATH, screenshotPath));
            // Copy new image
            Files.copy(Paths.get(imgFile.getAbsolutePath()),
                    Paths.get(PropertiesConfig.ROOT_PATH, screenshotPath));
            // Set screenshot in game object
            game.setScreenshot(screenshotPath);
        }
        if (path != null && gameFileIsChanged) {
            // Create dir if not exists
            if (!DirUtility.existsGameDir(game))
                Files.createDirectories(
                        Paths.get(PropertiesConfig.ROOT_PATH, DirUtility.getGameDirName(game)));
            String romPath =
                    DirUtility.getGameDirName(game) + "rom" + path.substring(path.lastIndexOf('.'));
            // Delete old rom if exists
            Files.walk(Paths.get(PropertiesConfig.ROOT_PATH, DirUtility.getGameDirName(game)))
                    .forEach(file -> {
                        if (DirUtility.getFileNameWithoutExtension(file).equals("rom"))
                            try {
                                Files.delete(file);
                            } catch (IOException e) {
                                System.out.println("Llega hasta aquí");
                                AppDialog.errorDialog("Error",
                                        "Se ha producido un error al crear el "
                                                + "directorio de recursos del juego.");
                            }
                    });
            // Copy new rom
            Files.copy(Paths.get(gameFile.getAbsolutePath()),
                    Paths.get(PropertiesConfig.ROOT_PATH, romPath));
            // Set path in game object
            game.setPath(romPath);
        }
    }

    // Move resources from old directory to new one
    private void moveResources() throws IOException {
        if (DirUtility.existsGameDir(oldGame)) {
            // Move directory to new location
            Files.move(Paths.get(PropertiesConfig.ROOT_PATH, DirUtility.getGameDirName(oldGame)),
                    Paths.get(PropertiesConfig.ROOT_PATH, DirUtility.getGameDirName(game)));
            // Set screenshot and path members
            Files.walk(Paths.get(PropertiesConfig.ROOT_PATH, DirUtility.getGameDirName(game)))
                    .forEach(file -> {
                        String fileNameWithoutExtension =
                                DirUtility.getFileNameWithoutExtension(file);
                        if (fileNameWithoutExtension.equals("cover"))
                            game.setScreenshot(DirUtility.getGameDirName(game) + "cover");
                        else if (fileNameWithoutExtension.equals("rom"))
                            game.setPath(DirUtility.getGameDirName(game) + "rom"
                                    + file.toString().substring(file.toString().lastIndexOf('.')));
                    });
        }
    }

    // Checks if year is valid
    private boolean yearIsValid(String year) {
        String regex = "^[12][0-9]{3}$";
        return year.matches(regex);
    }
}
